///////////////////////////////////////////////////////////////////////////////////////
/////////     openSMILE arff targets generic config file             //////////////////
///////// (c) audEERING UG (haftungsbeschränkt),                     //////////////////
/////////     All rights reserverd.                                  //////////////////
///////////////////////////////////////////////////////////////////////////////////////


//
// configuration of commandline options for target classes in an ARFF file
// (cArffSink)
//
// change this file to match your labels!
//

 ; name of class label
class[0].name = valenceRank
 ; list of nominal classes OR "numeric"
class[0].type = numeric
 ; the class label or value for the current instance
target[0].all = \cm[vR{?}:instance class label]

class[1].name = arousalRank
class[1].type = numeric
target[1].all = \cm[aR{?}:instance class label]

class[2].name = valenceValue
class[2].type = numeric
target[2].all = \cm[vV{?}:instance class label]

class[3].name = arousalValue
class[3].type = numeric
target[3].all = \cm[aV{?}:instance class label]

class[4].name = valenceVariance
class[4].type = numeric
target[4].all = \cm[vS{?}:instance class label]

class[5].name = arousalVariance
class[5].type = numeric
target[5].all = \cm[aS{?}:instance class label]


