///////////////////////////////////////////////////////////////////////////////////////
///////// > openSMILE configuration file, Geneva feature set <       //////////////////
/////////                                                            //////////////////
/////////  * written 2013 by Florian Eyben *                         //////////////////
/////////                                                            //////////////////
///////// (c) audEERING GmbH                                         //////////////////
/////////     All rights reserved                                    //////////////////
///////////////////////////////////////////////////////////////////////////////////////



;;;;;;;;;;;;;;;;;;;;; functionals / summaries ;;;;;;;;;;;;;;;

[componentInstances:cComponentManager]
instance[egemapsv01a_functionalsMVR].type=cFunctionals
instance[egemapsv01a_functionalsMeanUV].type=cFunctionals
instance[egemapsv01a_functionalsMVRVoiced].type = cFunctionals

[egemapsv01a_functionalsMVR:cFunctionals]
reader.dmLevel = egemapsv01a_lldSetNoF0AndLoudnessZ_smo
writer.dmLevel = egemapsv01a_functionalsMeanStddevZ
\{\cm[bufferModeRbConf]}
copyInputName = 1
\{\cm[frameModeFunctionalsConf]}
functionalsEnabled = Moments
Moments.variance = 0
Moments.stddev = 0
Moments.stddevNorm = 1
Moments.skewness = 0
Moments.kurtosis = 0
Moments.amean = 1
nonZeroFuncts = 0
masterTimeNorm = segment

[egemapsv01a_functionalsMeanUV:cFunctionals]
reader.dmLevel = egemapsv01a_lldSetSpectralZ_smo
writer.dmLevel = egemapsv01a_functionalsMeanUnvoiced
\{\cm[bufferModeRbConf]}
copyInputName = 1
\{\cm[frameModeFunctionalsConf]}
functionalsEnabled = Moments
Moments.variance = 0
Moments.stddev = 0
Moments.stddevNorm = 0
Moments.skewness = 0
Moments.kurtosis = 0
Moments.amean = 1
nonZeroFuncts = 1
masterTimeNorm = segment

[egemapsv01a_functionalsMVRVoiced:cFunctionals]
reader.dmLevel = egemapsv01a_lldSetNoF0AndLoudnessNz_smo;egemapsv01a_lldSetSpectralNz_smo
writer.dmLevel = egemapsv01a_functionalsMeanStddevVoiced
\{\cm[bufferModeRbConf]}
copyInputName = 1
\{\cm[frameModeFunctionalsConf]}
functionalsEnabled = Moments
Moments.variance = 0
Moments.stddev = 0
Moments.stddevNorm = 1
Moments.skewness = 0
Moments.kurtosis = 0
Moments.amean = 1
nonZeroFuncts = 1
masterTimeNorm = segment


