package com.opencosy.networktools.utils;


import com.opencosy.networktools.model.property.Property;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static com.opencosy.networktools.utils.Constants.*;

/**
 * Copy ffmpeg, openSMILE and config, when not exist
 */
public class Initialization {

    /**
     * Log variable
     */
    private static final Logger logger = LoggerFactory.getLogger(Initialization.class);

    /**
     * Try to detect config file in local directory.
     * If it not exist, then copy file from jar to local directory + "/config"
     *
     * @param path path to config directory
     */
    public static void detectProperties(String path) {
        String filePath = path + File.separator + CONFIG_PROPERTIES;
        PROPERTIES_PATH = filePath;
        if (new File(PROPERTIES_PATH).exists()) {
            logger.info("Config file found");
            return;
        }

        logger.info("Config file doesn't exist. Make copy from jar");
        URL inputUrl = Initialization.class.getResource("/" + CONFIG + "/" + CONFIG_PROPERTIES);
        File dest = new File(filePath);
        try {
            Files.createDirectories(Paths.get(path));
        } catch (IOException e) {
            logger.error("Can't create directory for config file", e);
        }
        try {
            FileUtils.copyURLToFile(inputUrl, dest);
        } catch (IOException e) {
            logger.error("Can't copy config from jar to directory", e);
        }
        Property.setProperties(PROPERTIES_PATH_TEXT, PROPERTIES_PATH);
        Property.getProperties();
    }

    /**
     * Try to detect OS depended ffmpeg file in local directory.
     * If it not exist, then copy file from jar to local directory + "/ffmpeg"
     *
     * @param path path to ffmpeg directory
     */
    public static void detectFFMPEG(String path) {
        String fileName = "";
        if (SystemUtils.IS_OS_MAC) {
            fileName = "ffmpeg";
            logger.info("System is " + MAC);
        } else if (SystemUtils.IS_OS_WINDOWS) {
            fileName = "ffmpeg.exe";
            logger.info("System is " + WINDOWS);
        } else if (SystemUtils.IS_OS_LINUX) {
            fileName = "linux_ffmpeg";
            logger.info("System is " + LINUX);
        }
        String filePath = path + File.separator + fileName;
        FFMPEG_PATH = filePath;
        if (new File(FFMPEG_PATH).exists()) {
            logger.info("FFMPEG found");
            return;
        }
        logger.info("FFMPEG not found. Make copy from jar");
        URL inputUrl = Initialization.class.getResource("/" + FFMPEG + "/" + fileName);
        File dest = new File(filePath);
        try {
            Files.createDirectories(Paths.get(path));
        } catch (IOException e) {
            logger.error("Can't create directory for ffmpeg", e);
        }
        try {
            FileUtils.copyURLToFile(inputUrl, dest);
        } catch (IOException e) {
            logger.error("Can't copy ffmpeg from jar to directory", e);
        }
        try {
            setPermission(dest);
        } catch (IOException e) {
            logger.error("Can't set permission to ffmpeg", e);
        }
        Property.setProperties(FFMPEG_PATH_TEXT, FFMPEG_PATH);
    }


    /**
     * Try to detect openSMILE file in local directory.
     * If it not exist, then copy file from jar to local directory + "/openSMILE"
     *
     * @param path path to openSMILE directory
     */
    public static void detectOpenSMILE(String path) {
        String fileName = "";
        if (SystemUtils.IS_OS_MAC) {
            fileName = File.separator + MAC;
            OS_PATH = path + OPENSMILE + File.separator + MAC + File.separator + "bin" + File.separator + OS_MAC_LINUX;
        } else if (SystemUtils.IS_OS_WINDOWS) {
            fileName = "/" + WINDOWS;
            OS_PATH = path + OPENSMILE + File.separator + WINDOWS + File.separator + OS_WIN;
        } else if (SystemUtils.IS_OS_LINUX) {
            fileName = File.separator + LINUX;
            OS_PATH = path + OPENSMILE + File.separator + LINUX + File.separator + OS_MAC_LINUX;
        }
        Property.setProperties(OS_PATH_TEXT, OS_PATH);
        File dir = new File(OS_PATH);
        if (dir.exists() || dir.listFiles() != null) {
            logger.info("openSMILE found");
            return;
        }

        logger.info("openSMILE not found. Make copy from jar");
        try {
            Files.createDirectories(Paths.get(path + File.separator + OPENSMILE));
        } catch (IOException e) {
            logger.error("Can't create directory for openSMILE", e);
        }
        JarFile jar = openJar();
        if (jar == null) {
            logger.error("Can't open jar file");
            return;
        }
        final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
        while (entries.hasMoreElements()) {
            final String name = entries.nextElement().getName();
            //copy osSMLILE
            copyOpenSMILE(name, fileName, path);
            //copy osConfig
            copyOSConfig(name, path);
        }
        try {
            jar.close();
        } catch (IOException e) {
            logger.error("Can't close jar file", e);
        }
    }


    /**
     * Try to detect images file in local directory.
     * If it not exist, then copy file from jar to local directory + "/image"
     *
     * @param path path to openSMILE directory
     */

    public static void detectImages(String path) {
        File dir = new File(path + File.separator + IMAGES);
        if (dir.exists() && dir.listFiles().length != 0) {
            logger.info("Images found");
            return;
        }
        logger.info("Images not found. Make copy from jar");
        try {
            Files.createDirectories(Paths.get(path + File.separator + IMAGES));
        } catch (IOException e) {
            logger.error("Can't create directory for images", e);
        }
        JarFile jar = openJar();
        if (jar == null) {
            logger.error("Can't open jar file");
            return;
        }
        final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
        while (entries.hasMoreElements()) {
            final String name = entries.nextElement().getName();
            copyImage(name, path);
        }
        try {
            jar.close();
        } catch (IOException e) {
            logger.error("Can't close jar file", e);
        }
    }

    public static void detectAED(String path) {
        File dir = new File(path + File.separator + VADDATA);
        if (dir.exists() && dir.listFiles().length != 0) {
            logger.info("AED found");
            return;
        }
        logger.info("aed not found. Make copy from jar");
        try {
            Files.createDirectories(Paths.get(path + File.separator + VADDATA));
        } catch (IOException e) {
            logger.error("Can't create directory for aed", e);
        }
        JarFile jar = openJar();
        if (jar == null) {
            logger.error("Can't open jar file");
            return;
        }
        final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
        while (entries.hasMoreElements()) {
            final String name = entries.nextElement().getName();
            copyAED(name, path);
        }
        try {
            jar.close();
        } catch (IOException e) {
            logger.error("Can't close jar file", e);
        }

        if (SystemUtils.IS_OS_LINUX) {
            VAD_PATH = path + VADDATA + File.separator + "util" + File.separator + "ldetect";
        } else if (SystemUtils.IS_OS_WINDOWS) {
            VAD_PATH = path + VADDATA + File.separator + "util" + File.separator + "detect.bat";
        }
        Property.setProperties(VAD_PATH_TEXT, VAD_PATH);
    }

    public static void delectLockFile(String path){
        File dir = new File(path);
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().contains(".lock")) {
                    file.delete();
                }
            }
        }
    }

    /**
     * Set on ffmpeg file permission to 700
     *
     * @param file ffmpeg file
     * @throws IOException exception
     */
    private static void setPermission(File file) throws IOException {
        if (!SystemUtils.IS_OS_WINDOWS) {
            Set<PosixFilePermission> perms = new HashSet<>();
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.OWNER_EXECUTE);
            Files.setPosixFilePermissions(file.toPath(), perms);
        }
    }

    private static JarFile openJar() {
        String jarPath = Initialization.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        JarFile jar = null;
        try {
            jarPath = URLDecoder.decode(jarPath, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("Can't encode path to jar", e);
        }
        final File jarFile = new File(jarPath);
        if (jarFile.isFile()) {  // Run with JAR file
            try {
                jar = new JarFile(jarFile);
            } catch (IOException e) {
                logger.error("Can't open jar file", e);
            }
        }
        return jar;
    }

    private static void copyAndPermission(URL inputUrl, File file) {
        if (inputUrl == null || file == null) {
            return;
        }
        try {
            FileUtils.copyURLToFile(inputUrl, file);
        } catch (IOException e) {
            logger.error("Can't copy files in openSMILE", e);
        }
        try {
            setPermission(file);
        } catch (IOException e) {
            logger.error("Can't set permission for openSMILE", e);
        }
    }

    private static void copyOpenSMILE(String name, String fileName, String path) {
        if (!name.startsWith(OPENSMILE + fileName)) { //filter according to the path
            return;
        }
        fileOrDirectory(name, path);
    }

    private static void copyOSConfig(String name, String path) {
        if (!name.startsWith(OPENSMILE + "/" + "osConfig")) {
            return;
        }
        File file = new File(path + name);
        if ((path + name).endsWith("/")) {
            try {
                Files.createDirectories(Paths.get(path + name).getParent());
            } catch (IOException e) {
                logger.error("Can't create subdirectory in openSMILE", e);
            }
        } else {
            URL inputUrl = Initialization.class.getResource("/" + name);
            copyAndPermission(inputUrl, file);
        }
    }

    private static void fileOrDirectory(String name, String path) {
        File file = new File(path + name);
        if (!(path + name).endsWith("/")) {
            URL inputUrl = Initialization.class.getResource("/" + name);
            copyAndPermission(inputUrl, file);
        } else {
            try {
                Files.createDirectories(Paths.get(path + name));
            } catch (IOException e) {
                logger.error("Can't create subdirectory in openSMILE", e);
            }
        }
    }

    private static void copyImage(String name, String path) {
        if (!name.startsWith(IMAGES)) { //filter according to the path
            return;
        }
        File file = new File(path + "/" + name);
        if ((path + name).endsWith("/")) {
            try {
                Files.createDirectories(Paths.get(path + File.separator + name));
            } catch (IOException e) {
                logger.error("Can't create subdirectory in images", e);
            }
        } else {
            URL inputUrl = Initialization.class.getResource("/" + name);
            copyAndPermission(inputUrl, file);
        }
    }

    private static void copyAED(String name, String path) {
        if (name.startsWith(VADDATA)) { //filter according to the path
            File file = new File(path + "/" + name);
            if ((path + name).endsWith("/")) {
                try {
                    Files.createDirectories(Paths.get(path + File.separator + name));
                } catch (IOException e) {
                    logger.error("Can't create subdirectory in images", e);
                }
            } else {
                URL inputUrl = Initialization.class.getResource("/" + name);
                copyAndPermission(inputUrl, file);
            }
        }
    }
}
