package com.opencosy.networktools.utils;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import com.opencosy.networktools.model.graph.Edge;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import javax.swing.*;
import java.util.Collection;
import java.util.List;

public class GraphView extends JFrame {
    private UndirectedGraph<String, DefaultEdge> graph =
            new SimpleGraph<>(DefaultEdge.class);


    public GraphView(List<Integer> nodes, List<Edge> edges, List<String> lcc) {

//        if (lcc != null) {
//            for (int i = 0; i < nodes.size(); i++) {
//                graph.addVertex(String.valueOf(nodes.get(i)) + "-" + lcc.get(i));
//            }
//        } else {
        for (int i = 0; i < nodes.size(); i++) {
            graph.addVertex(String.valueOf(nodes.get(i)));

        }


        for (Edge edge : edges) {
            graph.addEdge(String.valueOf(edge.getStart()), String.valueOf(edge.getEnd()));
        }

//        BronKerboschCliqueFinder cliqueFinder  = new BronKerboschCliqueFinder(graph);
//        Collection<Set> collections = cliqueFinder.getAllMaximalCliques();
//        Collection<Set> collections2 = cliqueFinder.getBiggestMaximalCliques();
//
//        System.out.println("BronKerboschCliqueFinder: Get all Maximal Cliques");
//        for (Set clique : collections){
//            System.out.println(clique.toString());
//        }
//
//        System.out.println("BronKerboschCliqueFinder: Get Biggest Maximal Cliques");
//        for (Set clique : collections2){
//            System.out.println(clique.toString());
//        }


    }

    public void paintGraph() {

        JGraphXAdapter<String, DefaultEdge> jgxAdapter = new JGraphXAdapter<>(graph);
        mxGraphComponent graphComponent = new mxGraphComponent(jgxAdapter);
        mxGraphModel graphModel = (mxGraphModel) graphComponent.getGraph().getModel();
        Collection<Object> cells = graphModel.getCells().values();

        mxUtils.setCellStyles(graphComponent.getGraph().getModel(),
                cells.toArray(), mxConstants.STYLE_ENDARROW, mxConstants.NONE);
        getContentPane().add(graphComponent);

        mxCircleLayout layout = new mxCircleLayout(jgxAdapter);


        layout.setRadius(500);
        layout.execute(jgxAdapter.getDefaultParent());
//        JGraphModelAdapter<String, DefaultEdge> adapter = new JGraphModelAdapter<>(graph);
//        JGraph jgraph = new JGraph(adapter);
//        JGraphLayout layout = new JGraphHierarchicalLayout(); // or whatever layouting algorithm
//        JGraphFacade facade = new JGraphFacade(jgraph);
//
//
//        layout.run(facade);
//        Map nested = facade.createNestedMap(false, false);
//        jgraph.getGraphLayoutCache().edit(nested);
//
//
//
//        JScrollPane sp = new JScrollPane(jgraph);
//        this.add(sp);

        this.pack();
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    public UndirectedGraph<String, DefaultEdge> getGraph() {
        return graph;
    }

    public void setGraph(UndirectedGraph<String, DefaultEdge> graph) {
        this.graph = graph;
    }

    public static class cc<V> extends DefaultEdge {
        private V v1;
        private V v2;
        private String label;

        public cc(V v1, V v2, String label) {
            this.v1 = v1;
            this.v2 = v2;
            this.label = label;
        }

        public V getV1() {
            return v1;
        }

        public V getV2() {
            return v2;
        }

        public String toString() {
            return label;
        }
    }
}

