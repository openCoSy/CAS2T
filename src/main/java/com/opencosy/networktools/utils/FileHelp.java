package com.opencosy.networktools.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import static com.opencosy.networktools.utils.Constants.*;

/**
 * File search class
 */
public class FileHelp {
    /**
     * Log variable
     */
    private static final Logger logger = LoggerFactory.getLogger(FileHelp.class);

    /**
     * search for preset directories
     *
     * @return true found all directories
     */
    public static boolean checkDirectories() {
        boolean flag;
        String downloadPath = DATA_PATH + File.separator + RAWDATA;
        String convertPath = DATA_PATH + File.separator + CONVERTEDDATA;
        String lldPath = DATA_PATH + File.separator + LLDDATA;
        String arffPath = DATA_PATH + File.separator + ARFFDATA;
        String outputPath = DATA_PATH + File.separator + OUTPUTDATA;
        flag = checkDirectory(downloadPath) && checkDirectory(convertPath) && checkDirectory(lldPath)
                && checkDirectory(arffPath) && checkDirectory(outputPath);
        return flag;
    }

    /**
     * @param path base directory
     * @return if exist true
     */
    private static boolean checkDirectory(String path) {
        boolean flag = true;
        File file = new File(path);
        if (!file.exists()) {
            flag = file.mkdir();
        }
        return flag;
    }

    /**
     * Find path to files
     *
     * @param id  video Id
     * @param col column in table
     * @return path to file
     */
    public static String checkFile(String id, int col) {
        String path = "";
        switch (col) {
            case 1:
                path = DATA_PATH + File.separator + RAWDATA + File.separator;
                break;
            case 2:
            case 3:
                path = DATA_PATH + File.separator + CONVERTEDDATA + File.separator;
                break;
            case 4:
                path = DATA_PATH + File.separator + LLDDATA + File.separator;
                break;
            case 5:
                path = DATA_PATH + File.separator + LLDDATA + File.separator;
                break;
        }

        String filePath = "-";
        File[] folder = new File(path).listFiles();
        if (folder != null && folder.length != 0) {
            for (File aFolder : folder) {
                switch (col) {
                    case 1:
                        if ((aFolder.toString().toLowerCase().endsWith("mp4") ||
                                aFolder.toString().toLowerCase().endsWith("webm") ||
                                aFolder.toString().toLowerCase().endsWith("3gp") ||
                                aFolder.toString().toLowerCase().endsWith("flv"))
                                && aFolder.getName().contains(id)) {
                            filePath = aFolder.toString();
                        }
                        break;
                    case 2:
                        if ((aFolder.toString().toLowerCase().endsWith("wav"))
                                && aFolder.getName().contains(id) && !aFolder.toString().contains("_m2")) {
                            filePath = aFolder.toString();
                        }
                        break;
                    case 3:
                        if ((aFolder.toString().toLowerCase().endsWith("wav"))
                                && aFolder.getName().contains(id + "_m2")) {
                            filePath = aFolder.toString();
                        }
                        break;
                    case 4:
                        if ((aFolder.toString().toLowerCase().endsWith("arff"))
                                && aFolder.getName().contains(id) && !aFolder.toString().contains("_m2")) {
                            filePath = aFolder.toString();
                        }
                        break;
                    case 5:
                        if ((aFolder.toString().toLowerCase().endsWith("arff"))
                                && aFolder.getName().contains(id + "_m2")
                                && aFolder.getParent().contains("lld")) {
                            filePath = aFolder.toString();
                        }
                        break;
                    default:
                        filePath = "-";
                        break;
                }
            }
            return filePath;
        } else {
            return "-";
        }
    }

    /**
     * run ffmpeg command
     *
     * @return true found/ false not found
     */
    public static boolean checkConverter() {
        try {
            ProcessBuilder pb = new ProcessBuilder(FFMPEG_PATH, "-version");
            pb.start();
            return true;
        } catch (IOException e) {
            logger.error("FFMPEG not found", e);
            return false;
        }
    }
}
