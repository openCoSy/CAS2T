package com.opencosy.networktools.utils;

import com.opencosy.networktools.model.graph.Edge;
import com.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static com.opencosy.networktools.utils.Constants.DATA_PATH;

/**
 * Class make csv table of graph
 */
public class CSV {
    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(CSV.class);

    /**
     * Save matrix to file
     *
     * @param nodes list of nodes
     * @param edges list of edges
     */
    public void writeToFile(List<Integer> nodes, List<Edge> edges) {
        try {
            final int[][] matrix = getMatrix(nodes, edges);
            final List<String> strMatrix = intMatrixToString(matrix);

            File file = new File(DATA_PATH + "/matrix.csv");
            FileWriter fw = new FileWriter(file);

            CSVWriter writer = new CSVWriter(fw, ',',
                    CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);

            for (String line : strMatrix) {
                String[] entries = line.split(" ");
                writer.writeNext(entries);
            }
            writer.close();
            fw.close();
        } catch (IOException e) {
            logger.error("Can't write matrix to file", e);
        }
    }

    private int[][] getMatrix(List<Integer> nodes, List<Edge> edges) {
        int[][] matrix = new int[nodes.size()][nodes.size()];
        List<Integer> id = new LinkedList<>();
        for (Edge edge : edges){
            id.add(edge.getStart());
        }
           // matrix[edge.getStart() - 1][edge.getEnd() - 1] = 1;
          //  matrix[edge.getEnd() - 1][edge.getStart() - 1] = 1;
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    for (Edge edge : edges) {
                    if ((i == edge.getStart() - 1 && j == edge.getEnd() - 1)
                            || (i == edge.getEnd() - 1 && j == edge.getStart() - 1)) {
                        matrix[i][j] = 1;
                    }
                }
            }
        }
        return matrix;
    }

    private List<String> intMatrixToString(int[][] matrix) {
        List<String> stringList = new LinkedList<>();
        StringBuilder temp = new StringBuilder();
        for (int[] aMatrix : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                temp.append(String.valueOf(aMatrix[j])).append(" ");
            }
            stringList.add(temp.toString());
            temp = new StringBuilder();
        }
        return stringList;
    }
}
