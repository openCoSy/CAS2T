package com.opencosy.networktools.utils;

public class Constants {
    public static String PATH = "";
    public static final String PREFIX = "https://www.youtube.com/watch?v=";
    //Properties
    public static String FFMPEG_PATH = "";
    public static String PROPERTIES_PATH = "";
    public static String OS_PATH = "";
    public static String DATABASE_PATH = "";
    public static String DATA_PATH = "";
    public static String RELATEDVIDEO_COUNT = "4";
   // public static String apiKey = "AIzaSyAodH3ZbsOMcjK-EKHpTSLvnBfYDDACuNU";
   public static String apiKey = "AIzaSyDaF-q8H168lS1PvjpJLU4qC-c5YUmQscI";
    public static String SAMPLE_RATE = "44100";

    public static boolean COMMENTS = false;

    public static String OS_CONFIG = "";


    //text constant
    public static final String CONFIG = "config";
    public static final String CONFIG_PROPERTIES = "config.properties";
    public static final String FFMPEG = "ffmpeg";
    public static final String MAC = "macOS";
    public static final String WINDOWS = "Windows";
    public static final String LINUX = "linux";
    public static final String IMAGES = "images";
    public static final String RAWDATA = "downloads";
    public static final String CONVERTEDDATA = "converted";
    public static final String VADDATA = "aed";
    public static final String ARFFDATA = "arff";
    public static final String LLDDATA = "lld";
    public static final String OUTPUTDATA = "output_aed";

    // Name of openSMILE bin
    public static final String OPENSMILE = "openSMILE";
    public static final String OS_MAC_LINUX = "SMILExtract";
    public static final String OS_WIN = "SMILExtract_Release.exe";

    //Name of properties
    public final static String APIKEY_TEXT = "apiKey";
    public final static String FFMPEG_PATH_TEXT = "ffmpeg";
    public final static String DATA_PATH_TEXT = "dataPath";
    public final static String DATABASE_PATH_TEXT = "databasePath";
    public final static String PROPERTIES_PATH_TEXT = "properties";
    public final static String RELATEDVIDEO_COUNT_TEXT = "relatedVideo";
    public final static String SAMPLE_RATE_TEXT = "sampleRate";
    public final static String OS_PATH_TEXT = "osPath";
    public final static String OS_CONFIG_TEXT = "osConfig";

    // images
    public final static String TICK_PATH = "ok.png";
    public final static String CROSS_PATH = "error.png";

    //VAD Model building

    public static String TRAIN_PATH = "";
    public static String DEVEL_PATH = "";
    public static String OUTPUT_PATH = "";
    public static String MODEL_PATH = "";
    public static String VAD_PATH = "";
    public final static String MODEL_PATH_TEXT = "AEDModel";
    public final static String TRAIN_PATH_TEXT = "trainSet";
    public final static String VAD_PATH_TEXT = "AEDPath";

    public static boolean FIRST_START = true;

}
