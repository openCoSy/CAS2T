package com.opencosy.networktools.utils.process;


import com.opencosy.networktools.model.video.MyVideo;

import java.io.File;
import java.io.IOException;
import java.util.Observable;

import static com.opencosy.networktools.utils.Constants.*;

/**
 * Class converter from audio to wav
 */
public class Converter extends Observable {

    private String output = "Converting";
    private MyVideo video = null;
    private boolean stopped = false;

    public void convert(String path) throws IOException {
        if (!stopped) {
            output = replaceToWav(path);
            File dir = new File(DATA_PATH + File.separator + CONVERTEDDATA);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            ProcessBuilder pb = new ProcessBuilder(FFMPEG_PATH, "-i", path, "-ar", SAMPLE_RATE, output);
            pb.start();
            setFinish(output);
            setStopped(true);
        }
    }

    private synchronized void setFinish(String finish) {
        synchronized (this) {
            this.output = finish;
        }
        setChanged();
        notifyObservers();
    }

    public String getFinish() {
        synchronized (this) {
            return this.output;
        }
    }

    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    public boolean getStopped() {
        return this.stopped;
    }

    public MyVideo getVideo() {
        return video;
    }

    public void setVideo(MyVideo video) {
        this.video = video;
    }

    private String replaceToWav(String path) {
        String output = "";
        if (path.contains("webm")) {
            output = path.replace("webm", "wav");
        }
        if (path.contains("mp4")) {
            output = path.replace("mp4", "wav");
        }
        if (path.contains("3gp")) {
            output = path.replace("3gp", "wav");
        }
        if (path.contains("3gpp")) {
            output = path.replace("3gpp", "wav");
        }
        if (path.contains("flv")) {
            output = path.replace("flv", "wav");
        }
        output = output.replace(RAWDATA, CONVERTEDDATA);
        return output;
    }
}

