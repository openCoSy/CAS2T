package com.opencosy.networktools.utils.process;


import com.github.axet.vget.VGet;
import com.github.axet.vget.info.VGetParser;
import com.github.axet.vget.info.VideoFileInfo;
import com.github.axet.vget.info.VideoInfo;
import com.github.axet.vget.vhs.VimeoInfo;
import com.github.axet.vget.vhs.YouTubeInfo;
import com.github.axet.wget.SpeedInfo;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.URLInfo;
import com.github.axet.wget.info.ex.DownloadInterruptedError;
import com.opencosy.networktools.model.video.MyVideo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.opencosy.networktools.utils.Constants.*;


public class Download extends Observable implements Runnable {

    private String output = "Downloading";
    private MyVideo video = null;
    private AtomicBoolean stop = new AtomicBoolean(false);
    private boolean stoped;
    private String id;


    public String download() {
        List<VideoFileInfo> list = null;
        String finalPath = null;
        File file = null;
        File file2 = null;
        try {
            String path = DATA_PATH + File.separator + RAWDATA + File.separator;
            file = new File(path + id + File.separator);
            file.mkdirs();

            URL web = null;
            try {
                web = new URL(PREFIX + id);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            VGetParser parser = VGet.parser(web);
            VideoInfo videoinfo = parser.info(web);
            VGetStatus notify = new VGetStatus(videoinfo);
            VGet v = new VGet(videoinfo, file);
            v.extract(parser, stop, notify);
            list = videoinfo.getInfo();


            if (list == null) {
                return "-";
            }
            if (list.size() == 1) {
                //  setFinish("Downloading");
                final String finalExt = getExtension(list.get(0).getContentType());
                finalPath = path + id + finalExt;
                final String outputTemp = finalPath + ".lock";
                file2 = new File(outputTemp);
                list.get(0).targetFile = file2;
                // v.download(user, stop, notify);
            } else {
                for (VideoFileInfo d : list) {
                    if (d.getContentType().contains("audio/")) {
                  //      setFinish("DOWNLOADING");
                        final String finalExt = getExtension(d.getContentType());
                        finalPath = path + id + finalExt;
                        final String outputTemp = finalPath + ".lock";
                        file2 = new File(outputTemp);
                        d.targetFile = file2;
                    }
                }
            }

            v.download(parser, stop, notify);
            if (list.get(0).getState().equals(URLInfo.States.DONE)) {
                file2.renameTo(file2.toPath().resolveSibling(finalPath).toFile());
            }

            for (File file1 : file.listFiles()) {
                file1.delete();
            }
            file.delete();
            setFinish(finalPath);
            //setStoped(true);
            return finalPath;
        } catch (DownloadInterruptedError e) {
            //if (list.get(0).getState().equals(URLInfo.States.DONE)) {
            //    file2.renameTo(file2.toPath().resolveSibling(finalPath).toFile());
            //}
            setFinish("-");
            for (File file1 : file.listFiles()) {
                file1.delete();
            }
            file.delete();
            file2.delete();
            setStoped(true);
            return "-";
        }
    }

    private String getExtension(String content) {
        String extension = "";
        if (content.contains("audio/")) {
            extension = content.replace("audio/", ".");
        }
        if (content.contains("video/")) {
            extension = content.replace("video/", ".");
        }
        return extension;
    }

    private synchronized void setFinish(String finish) {
        synchronized (this) {
            this.output = finish;
        }
        setChanged();
        notifyObservers();
    }

    public void setStop() {
        synchronized (this.stop) {
            this.stop.set(true);
            this.stop.notifyAll();
//            this.stop.compareAndSet(this.stop.get(), stop);
        }
    }

    public AtomicBoolean getStop() {
        return this.stop;
    }

    public String getFinish() {
        synchronized (this) {
            return this.output;
        }
    }

    public MyVideo getVideo() {
        return video;
    }

    public void setVideo(MyVideo video) {
        this.video = video;
    }

    public boolean isStoped() {
        return stoped;
    }

    public void setStoped(boolean stoped) {
        this.stoped = stoped;
    }

    @Override
    public void run() {
        download();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private class VGetStatus implements Runnable {
        VideoInfo videoinfo;
        long last;

        Map<VideoFileInfo, SpeedInfo> map = new HashMap<>();

        VGetStatus(VideoInfo i) {
            this.videoinfo = i;
        }

        SpeedInfo getSpeedInfo(VideoFileInfo dinfo) {
            SpeedInfo speedInfo = map.get(dinfo);
            if (speedInfo == null) {
                speedInfo = new SpeedInfo();
                speedInfo.start(dinfo.getCount());
                map.put(dinfo, speedInfo);
            }
            return speedInfo;
        }

        @Override
        public void run() {
            List<VideoFileInfo> dinfoList = videoinfo.getInfo();

            // notify app or save download state
            // you can extract information from DownloadInfo info;
            switch (videoinfo.getState()) {
                case EXTRACTING:
                case EXTRACTING_DONE:
                case DONE:
                    if (videoinfo instanceof YouTubeInfo) {
                        YouTubeInfo i = (YouTubeInfo) videoinfo;
                        System.out.println(videoinfo.getState() + " " + i.getVideoQuality());
                    } else if (videoinfo instanceof VimeoInfo) {
                        VimeoInfo i = (VimeoInfo) videoinfo;
                        System.out.println(videoinfo.getState() + " " + i.getVideoQuality());
                    } else {
                        System.out.println("downloading unknown quality");
                    }
                    for (VideoFileInfo d : videoinfo.getInfo()) {
                        SpeedInfo speedInfo = getSpeedInfo(d);
                        speedInfo.end(d.getCount());
                        System.out.println(String.format("file:%d - %s (%s)", dinfoList.indexOf(d), d.targetFile,
                                formatSpeed(speedInfo.getAverageSpeed())));
                    }

                    break;
                case ERROR:
                    System.out.println(videoinfo.getState() + " " + videoinfo.getDelay());

                    if (dinfoList != null) {
                        for (DownloadInfo dinfo : dinfoList) {
                            System.out.println("file:" + dinfoList.indexOf(dinfo) + " - " + dinfo.getException() + " delay:"
                                    + dinfo.getDelay());
                        }
                    }
                    break;
                case RETRYING:
                    System.out.println(videoinfo.getState() + " " + videoinfo.getDelay());

                    if (dinfoList != null) {
                        for (DownloadInfo dinfo : dinfoList) {
                            System.out.println("file:" + dinfoList.indexOf(dinfo) + " - " + dinfo.getState() + " "
                                    + dinfo.getException() + " delay:" + dinfo.getDelay());
                        }
                    }
                    break;
                case DOWNLOADING:
                    long now = System.currentTimeMillis();
                    if (now - 1000 > last) {
                        last = now;
                        String parts = "";
                        for (VideoFileInfo dinfo : dinfoList) {
                            SpeedInfo speedInfo = getSpeedInfo(dinfo);
                            speedInfo.step(dinfo.getCount());
                            List<DownloadInfo.Part> pp = dinfo.getParts();
                            if (pp != null) {
                                // multipart download
                                for (DownloadInfo.Part p : pp) {
                                    if (Objects.equals(p.getState().toString(), VideoInfo.States.DOWNLOADING.toString())) {
                                        parts += String.format("part#%d(%.2f) ", p.getNumber(),
                                                p.getCount() / (float) p.getLength());
                                    }
                                }
                            }
                            float size = (float) dinfo.getLength() / 1024 / 1024;
                            setFinish(String.format("file size: %.2f MB - %s %.2f %% %s (%s)", size,
                                    videoinfo.getState().toString().toLowerCase(), (dinfo.getCount()
                                            / (float) dinfo.getLength()) * 100, parts,
                                    formatSpeed(speedInfo.getCurrentSpeed())));
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        String formatSpeed(long s) {
            if (s > 0.1 * 1024 * 1024 * 1024) {
                float f = s / 1024f / 1024f / 1024f;
                return String.format("%.1f GB/s", f);
            } else if (s > 0.1 * 1024 * 1024) {
                float f = s / 1024f / 1024f;
                return String.format("%.1f MB/s", f);
            } else {
                float f = s / 1024f;
                return String.format("%.1f kb/s", f);
            }
        }
    }
}