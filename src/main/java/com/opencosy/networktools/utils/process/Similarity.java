package com.opencosy.networktools.utils.process;


import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.util.LinkedList;

public class Similarity {

    public static LinkedList<Double> processSimilarity(String path, String className) throws Exception {
        ConverterUtils.DataSource dataSource = new ConverterUtils.DataSource(path);
        Instances data = dataSource.getDataSet();
        Instances newData = setFilter(setFilter(data, "1"), "1");
        newData.setClassIndex(newData.numAttributes() - 1);
        int classIndex = newData.classIndex();

        double[] doubleArray = new double[newData.numAttributes() - 1];
        if (!className.equals("")) {
            double counter;
            int anzahl;
            for (int i = 0; i < newData.numAttributes() - 1; i++) {
                anzahl = 0;
                counter = 0;
                double[] column = newData.attributeToDoubleArray(i);
                for (int j = 0; j < newData.size(); j++) {
                    Instance instance = newData.instance(j);
                    if (instance.stringValue(classIndex).equals(className)) {
                        counter = counter + column[j];
                        anzahl++;
                    }
                }
                doubleArray[i] = counter / anzahl;
            }
        }

        LinkedList<Double> cor = new LinkedList<>();
        double mean = mean(doubleArray);
        double deviation = deviation(doubleArray, mean);

        for (double aDoubleArray : doubleArray) {
            cor.add((aDoubleArray - mean) / deviation);
            System.out.println(aDoubleArray + " --------------- " + (aDoubleArray - mean) / deviation);
        }
        System.out.println("-----------------");
        return cor;
    }

    private static double mean(double[] doubleArray) {
        double meanValue = 0.0;
        for (double aDoubleArray : doubleArray) {
            meanValue = meanValue + aDoubleArray;
        }
        meanValue = meanValue / (double) doubleArray.length;
        return meanValue;
    }

    private static double deviation(double[] doubleArray, double mean) {
        double deviation = 0.0;
        for (double aDoubleArray : doubleArray) {
            deviation = deviation + (aDoubleArray - mean) * (aDoubleArray - mean);
        }
        double temp = doubleArray.length - 1.0;
        return Math.sqrt((1.0 / temp) * deviation);
    }


    private static Instances setFilter(Instances data, String row) throws Exception {
        String[] options2 = new String[2];
        options2[0] = "-R";                                    // "range"
        options2[1] = row;                                     // first attribute
        Remove remove = new Remove();                         // new instance of filter
        remove.setOptions(options2);                           // set options
        remove.setInputFormat(data);                          // inform filter about dataset **AFTER** setting options
        return Filter.useFilter(data, remove);   // apply filter
    }
}
