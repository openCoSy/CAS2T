package com.opencosy.networktools.utils.process;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static com.opencosy.networktools.utils.Constants.*;

public class FeatureExtraction {
    /**
     * Log variable
     */
    private static final Logger logger = LoggerFactory.getLogger(FeatureExtraction.class);

    public static String processARFF(String path, boolean lld) {
        String output;

        output = path.replace(CONVERTEDDATA, LLDDATA);
        output = output.replace(".wav", ".arff");
        File file = new File(output);
        if (file.exists()) {
            file.delete();
        }
        File name = new File(path);
        File dir = new File(new File(output).getParent());
        dir.mkdir();
        if (!dir.exists()) {
            try {
                Files.createDirectory(dir.toPath());
            } catch (IOException e) {
                logger.error("Can't create directory", e);
            }
        }
        //SMILExtract -C config/emobase.conf -I input.wav -O output.arff
        ProcessBuilder pb;
        if (lld) {
            pb = new ProcessBuilder(OS_PATH, "-C", OS_CONFIG, "-I", path, "-D", output, "-N", name.getName()).inheritIO();
        } else {
            pb = new ProcessBuilder(OS_PATH, "-C", OS_CONFIG, "-I", path, "-O", output, "-N", name.getName()).inheritIO();
        }
        try {
            pb.start();
        } catch (IOException e) {
            logger.error("Can't start process", e);
        }
        return output;
    }

    public static void processARFF(String wav, String config, String output) {
        ProcessBuilder pb = new ProcessBuilder(OS_PATH, "-C", config, "-I", wav, "-D", output).inheritIO();
        try {
            pb.start();
        } catch (IOException e) {
            logger.error("Can't start process", e);
        }
    }
}
