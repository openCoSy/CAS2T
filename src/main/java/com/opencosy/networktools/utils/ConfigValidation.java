package com.opencosy.networktools.utils;


import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeRequestInitializer;
import com.google.api.services.youtube.model.VideoListResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.opencosy.networktools.utils.Constants.*;
import static org.apache.commons.lang3.StringUtils.isNumeric;

/**
 * Check correctness of property file
 */
public class ConfigValidation {
    /**
     * Log variable
     */
    private static final Logger logger = LoggerFactory.getLogger(ConfigValidation.class);

    /**
     * call ffmpeg proccess
     *
     * @return true = find bin file and can be executed
     * false = file not found  or executed
     */
    public static boolean checkFFMPEG() {
        try {
            ProcessBuilder pb = new ProcessBuilder(FFMPEG_PATH, "-version");
            pb.start();
        } catch (IOException e) {
            logger.error("Path to ffmpeg isn't correct or can't be executed", e);
            return false;
        }
        return true;
    }

    /**
     * call openSMILE proccess
     *
     * @return true = find bin file and can be executed
     * false = file not found  or executed
     */
    public static boolean checkOpenSMILE() {
        try {
            ProcessBuilder pb = new ProcessBuilder(OS_PATH);
            pb.start();
        } catch (IOException e) {
            logger.error("Path to openSMILE isn't correct or can't be executed", e);
            return false;
        }
        return true;
    }

    /**
     * Check sample rate, must be integer
     *
     * @return true = contain only digits
     * false = not a number
     */
    public static boolean checkSampleRate() {
        return isNumeric(SAMPLE_RATE);
    }

    /**
     * Check count of related video, must be integer
     *
     * @return true = contain only digits
     * false = not a number
     */
    public static boolean checkRelatedVideoCount() {
        return isNumeric(RELATEDVIDEO_COUNT);
    }

    /**
     * Search for "config.properties" file
     *
     * @return true = found
     * false = not found
     */
    public static boolean checkConfig() {
        return PROPERTIES_PATH.endsWith(CONFIG_PROPERTIES);
    }

    /**
     * Check database file
     *
     * @return true = found
     * false = not found
     */
    //TODO check connect to database
    public static boolean checkDatabase() {
        return DATABASE_PATH.endsWith(".db") && !DATABASE_PATH.equals("");
    }

    /**
     * Check folder for data
     *
     * @return true = found
     * false = not found
     */
    public static boolean checkData() {
        File data = new File(DATA_PATH);
        if (!data.isDirectory() || !data.exists()) {
            if (!DATA_PATH.equals("")) {
                try {
                    Files.createDirectories(Paths.get(DATA_PATH));
                    return true;
                } catch (IOException e) {
                    logger.error("Cann't create data path", e);
                }
            }
            return false;
        }
        return true;
    }

    /**
     * Connect to Youtube API with key from config
     *
     * @return true = connect is ok
     * false = key is false or connection failure
     */
    public static boolean checkYoutubeAPIKey() {
        YouTube youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), httpRequest -> {

        }).setYouTubeRequestInitializer(new YouTubeRequestInitializer(apiKey))
                .setApplicationName("youtube-cmdline-search-sample").build();
        VideoListResponse videoListResponse = null;
        try {
            videoListResponse = youtube.videos().
                    list("id").setId("1NDAW8d1vx0").execute();
            return videoListResponse.getItems().size() != 0;
        } catch (IOException e) {
            logger.error("Youtube API key isn't correct or can't connect to Youtube API", e);
            return false;
        }
//        return true;
    }

    /**
     * search for config file for openSMILE
     *
     * @return true find file with .conf / false file not find
     */
    public static boolean checkOSConfig() {
        return OS_CONFIG.endsWith(".conf");
    }

    /**
     * search for model file for WEKA
     *
     * @return true find file with .model / false file not find
     */
    public static boolean checkModel() {
        return MODEL_PATH.endsWith(".model") || MODEL_PATH.equals("");
    }

    /**
     * search for train file for WEKA
     *
     * @return true find file with .arff / false file not find
     */
    public static boolean checkTrain() {
        return TRAIN_PATH.endsWith(".arff") || TRAIN_PATH.equals("");
    }

    /**
     * search for detect file for EAD
     *
     * @return true find file with detect / false file not find
     */
    public static boolean checkVAD() {
        return VAD_PATH.contains("detect") || VAD_PATH.equals("");
    }
}
