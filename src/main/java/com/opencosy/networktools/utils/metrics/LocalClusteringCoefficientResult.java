package com.opencosy.networktools.utils.metrics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class LocalClusteringCoefficientResult<V> {

    private Map<V, Double> raw;
    private double averageClusteringCoefficient;

    LocalClusteringCoefficientResult(Map<V, Double> raw) {
        this.raw = raw;
        calculateAverageClusteringCoefficient();
    }

    private void calculateAverageClusteringCoefficient() {
        double total = 0.0;
        for (Double value : raw.values()) {
            if (value != null) {
                total += value;
            }
        }
        averageClusteringCoefficient = total / raw.size();
    }

    public Double get(V v) {
        return raw.get(v);
    }

    public List<Map.Entry<V, Double>> getSorted() {
        List<Map.Entry<V, Double>> entryList = new ArrayList<>(raw.entrySet());
        Collections.sort(entryList, (o1, o2) -> {
            // Note that the order is reversed
            return Double.compare(o2.getValue(), o1.getValue());
        });
        return entryList;
    }

    public double getAverageClusteringCoefficient() {
        return averageClusteringCoefficient;
    }

}
