package com.opencosy.networktools.utils;

import com.opencosy.networktools.view.main.MainController;
import com.opencosy.networktools.model.database.DBConnector;
import com.opencosy.networktools.model.video.MyVideo;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import static com.opencosy.networktools.utils.Constants.DATABASE_PATH;

public class TableUtils {
    /**
     * Log variable
     */
    private static final Logger logger = LoggerFactory.getLogger(TableUtils.class);

    private static NumberFormat numberFormatter = NumberFormat.getNumberInstance();


    /**
     * Install the keyboard handler:
     * + CTRL + C = copy to clipboard
     * + CTRL + V = paste to clipboard
     *
     * @param table
     */
    public static void installCopyPasteHandler(TableView<MyVideo> table, DBConnector connector) {
        // install copy/paste keyboard handler
        table.setOnKeyPressed(new TableKeyEventHandler(table, connector));
    }


    /**
     * Copy/Paste keyboard event handler.
     * The handler uses the keyEvent's source for the clipboard data. The source must be of type TableView.
     */
    public static class TableKeyEventHandler implements EventHandler<KeyEvent> {

        KeyCodeCombination copyKeyCodeCombination = new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_ANY);
        KeyCodeCombination pasteKeyCodeCombination = new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_ANY);
        KeyCodeCombination selectAllCodeCombination = new KeyCodeCombination(KeyCode.A, KeyCombination.SHORTCUT_ANY);
        KeyCodeCombination unselectAllCodeCombination = new KeyCodeCombination(KeyCode.U, KeyCombination.SHORTCUT_ANY);
        KeyCodeCombination revertAllCodeCombination = new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_ANY);
        KeyCodeCombination selectCheckedCombination = new KeyCodeCombination(KeyCode.SPACE);
        KeyCodeCombination openCombination = new KeyCodeCombination(KeyCode.ENTER);
        TableView<MyVideo> tableView;
        DBConnector dbConnector;

        public TableKeyEventHandler(TableView<MyVideo> table, DBConnector connector) {
            tableView = table;
            dbConnector = connector;
        }


        public void writeCheckboxToDB(TableView<MyVideo> table, DBConnector connector) {
            Map<String, Integer> map = new HashMap<>();
            connector.connect(DATABASE_PATH, false);
            for (int i = 0; i < table.getItems().size(); i++) {
                map.put(table.getItems().get(i).getVideoId(), table.getItems().get(i).getSelected() ? 1 : 0);
            }
            connector.changeSelectes(map);
            connector.closeDB();
            synchronized (this) {
                MainController.singleItem = true;
            }
        }

        public void handle(final KeyEvent keyEvent) {
            if (copyKeyCodeCombination.match(keyEvent)) {
                if (keyEvent.getSource() instanceof TableView) {
                    // copy to clipboard
                    copySelectionToClipboard((TableView<?>) keyEvent.getSource());

                    // event is handled, consume it
                    keyEvent.consume();
                }
            } else if (pasteKeyCodeCombination.match(keyEvent)) {

                if (keyEvent.getSource() instanceof TableView) {
                    // copy to clipboard
                    pasteFromClipboard((TableView<?>) keyEvent.getSource());
                    // event is handled, consume it
                    keyEvent.consume();
                }
            } else if (selectAllCodeCombination.match(keyEvent)) {
                if (keyEvent.getSource() instanceof TableView) {
                    synchronized (this) {
                        MainController.singleItem = false;
                    }
                    selectAllCheckbox((TableView<?>) keyEvent.getSource());
                    writeCheckboxToDB(tableView, dbConnector);
                    keyEvent.consume();
                }
            } else if (unselectAllCodeCombination.match(keyEvent)) {
                if (keyEvent.getSource() instanceof TableView) {
                    synchronized (this) {
                        MainController.singleItem = false;
                    }
                    unselectAllCheckbox((TableView<?>) keyEvent.getSource());
                    writeCheckboxToDB(tableView, dbConnector);
                    keyEvent.consume();
                }

            } else if (revertAllCodeCombination.match(keyEvent)) {
                if (keyEvent.getSource() instanceof TableView) {
                    synchronized (this) {
                        MainController.singleItem = false;
                    }
                    revertAllCheckbox((TableView<?>) keyEvent.getSource());
                    writeCheckboxToDB(tableView, dbConnector);
                    keyEvent.consume();
                }
            } else if (selectCheckedCombination.match(keyEvent)) {
                if (keyEvent.getSource() instanceof TableView) {
                    selectChecked((TableView<?>) keyEvent.getSource());
                    keyEvent.consume();
                }
            } else if (openCombination.match(keyEvent)) {
                if (keyEvent.getSource() instanceof TableView) {
                    openItem((TableView<?>) keyEvent.getSource());
                    keyEvent.consume();
                }
            }
        }


    }

    /**
     * Get table selection and copy it to the clipboard.
     *
     * @param table
     */
    public static void copySelectionToClipboard(TableView<?> table) {

        StringBuilder clipboardString = new StringBuilder();

        ObservableList<TablePosition> positionList = table.getSelectionModel().getSelectedCells();

        int prevRow = -1;

        for (TablePosition position : positionList) {

            int row = position.getRow();
            int col = position.getColumn();

            // determine whether we advance in a row (tab) or a column
            // (newline).
            if (prevRow == row) {
                clipboardString.append('\t');
            } else if (prevRow != -1) {
                clipboardString.append('\n');
            }

            // create string from cell
            String text = "";

            Object observableValue = table.getColumns().get(col).getCellObservableValue(row);

            // null-check: provide empty string for nulls
            if (observableValue == null) {
                text = "";
            } else if (observableValue instanceof DoubleProperty) { // TODO: handle boolean etc
                text = numberFormatter.format(((DoubleProperty) observableValue).get());
            } else if (observableValue instanceof IntegerProperty) {
                text = numberFormatter.format(((IntegerProperty) observableValue).get());
            } else if (observableValue instanceof StringProperty) {
                text = ((StringProperty) observableValue).get();
            } else if (observableValue instanceof ObjectProperty) {
                text = (String) ((ObjectProperty) observableValue).get();
            } else {
                System.out.println("Unsupported observable value: " + observableValue);
            }

            // add new item to clipboard
            clipboardString.append(text);

            // remember previous
            prevRow = row;
        }

        // create clipboard content
        final ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(clipboardString.toString());

        // set clipboard content
        Clipboard.getSystemClipboard().setContent(clipboardContent);


    }

    public static void pasteFromClipboard(TableView<?> table) {
        // abort if there's not cell selected to start with
        if (table.getSelectionModel().getSelectedCells().size() == 0) {
            return;
        }
        // get the cell position to start with
        TablePosition pasteCellPosition = table.getSelectionModel().getSelectedCells().get(0);
        System.out.println("Pasting into cell " + pasteCellPosition);
        String pasteString = Clipboard.getSystemClipboard().getString();
        System.out.println(pasteString);
        int rowClipboard = -1;

        StringTokenizer rowTokenizer = new StringTokenizer(pasteString, "\n");
        while (rowTokenizer.hasMoreTokens()) {
            rowClipboard++;
            String rowString = rowTokenizer.nextToken();
            StringTokenizer columnTokenizer = new StringTokenizer(rowString, "\t");
            int colClipboard = -1;
            while (columnTokenizer.hasMoreTokens()) {
                colClipboard++;
                // get next cell data from clipboard
                String clipboardCellContent = columnTokenizer.nextToken();
                // calculate the position in the table cell
                int rowTable = pasteCellPosition.getRow() + rowClipboard;
                int colTable = pasteCellPosition.getColumn() + colClipboard;
                // skip if we reached the end of the table
                if (rowTable >= table.getItems().size()) {
                    continue;
                }
                if (colTable >= table.getColumns().size()) {
                    continue;
                }

                // System.out.println( rowClipboard + "/" + colClipboard + ": " + cell);

                // get cell
                TableColumn tableColumn = table.getColumns().get(colTable);
                ObservableValue observableValue = tableColumn.getCellObservableValue(rowTable);

                System.out.println(rowTable + "/" + colTable + ": " + observableValue);

                // TODO: handle boolean, etc
                if (observableValue instanceof DoubleProperty) {
                    try {

                        double value = numberFormatter.parse(clipboardCellContent).doubleValue();
                        ((DoubleProperty) observableValue).set(value);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (observableValue instanceof IntegerProperty) {
                    try {
                        int value = NumberFormat.getInstance().parse(clipboardCellContent).intValue();
                        ((IntegerProperty) observableValue).set(value);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (observableValue instanceof StringProperty) {
                    ((StringProperty) observableValue).set(clipboardCellContent);
                } else {
                    System.out.println("Unsupported observable value: " + observableValue);
                }
                System.out.println(rowTable + "/" + colTable);
            }
        }
    }

    public static void selectAllCheckbox(TableView<?> source) {
        for (Object video : source.getItems()) {
            if (video instanceof MyVideo) {
                ((MyVideo) video).setSelected(true);
            }
        }
        source.refresh();
    }

    public static void unselectAllCheckbox(TableView<?> source) {
        for (Object video : source.getItems()) {
            if (video instanceof MyVideo) {
                ((MyVideo) video).setSelected(false);
            }
        }
        source.refresh();
    }

    public static void revertAllCheckbox(TableView<?> source) {
        for (Object video : source.getItems()) {
            if (video instanceof MyVideo) {
                ((MyVideo) video).setSelected(!((MyVideo) video).getSelected());
            }
        }
        source.refresh();
    }

    public static void selectChecked(TableView<?> source) {
        ObservableList<TablePosition> positionList = source.getSelectionModel().getSelectedCells();
        for (TablePosition position : positionList) {
            int row = position.getRow();
            Object video = source.getItems().get(row);
            if (video instanceof MyVideo) {
                ((MyVideo) video).setSelected(!((MyVideo) video).getSelected());
            }
        }
        source.refresh();
    }

    public static void openItem(TableView<?> source) {
        @SuppressWarnings("rawtypes")
        TablePosition pos;
        pos = source.getSelectionModel().getSelectedCells().get(0);
        int row = pos.getRow();
        int col = pos.getColumn();
        @SuppressWarnings("rawtypes")
        TableColumn column = pos.getTableColumn();
        Object value = column.getCellData(row);
        String val = "";
        if (value != null) {
            val = value.toString();
            if (col == 3 || col == 4 || col == 5 || col == 6 || col == 7) {
                if (Desktop.isDesktopSupported()) {
                    try {
                        File file = new File(val);
                        if (file.exists()) {
                            Desktop.getDesktop().open(file);
                        }
                    } catch (IOException e) {
                        logger.error("Can't open file", e);
                    }
                }
            }
            if (col == 2) {
                if (source.getSelectionModel().getSelectedItem() != null) {
//                    if(Desktop.isDesktopSupported()) {
//                        try {
//                            Desktop.getDesktop().browse(URI.create(Constants.PREFIX + val));
//                        } catch (IOException e) {
//                            logger.error("Can't open url", e);
//                        }
//                    } else {
//                        Runtime runtime = Runtime.getRuntime();
//                        try {
//                            runtime.exec("xdg-open " + Constants.PREFIX + val);
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//                    }

                    String url = Constants.PREFIX + val;
                    String os = System.getProperty("os.name").toLowerCase();
                    Runtime rt = Runtime.getRuntime();

                    try{

                        if (os.contains("win")) {

                            // this doesn't support showing urls in the form of "page.html#nameLink"
                            rt.exec( "rundll32 url.dll,FileProtocolHandler " + url);

                        } else if (os.contains("mac")) {

                            rt.exec( "open " + url);

                        } else if (os.contains("nix") || os.contains("nux")) {

                            // Do a best guess on unix until we get a platform independent way
                            // Build a list of browsers to try, in this order.
                            String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
                                    "netscape","opera","links","lynx", "chrome","google-chrome"};

                            // Build a command string which looks like "browser1 "url" || browser2 "url" ||..."
                            StringBuilder cmd = new StringBuilder();
                            for (int i=0; i<browsers.length; i++)
                                cmd.append(i == 0 ? "" : " || ").append(browsers[i]).append(" \"").append(url).append("\" ");

                            rt.exec(new String[] { "sh", "-c", cmd.toString() });

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}