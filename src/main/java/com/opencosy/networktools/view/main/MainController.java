package com.opencosy.networktools.view.main;

import com.google.api.services.youtube.model.Comment;
import com.google.api.services.youtube.model.Video;
import com.google.common.io.Files;
import com.opencosy.networktools.model.audio.Segment;
import com.opencosy.networktools.model.database.DBConnector;
import com.opencosy.networktools.model.graph.Edge;
import com.opencosy.networktools.model.property.Property;
import com.opencosy.networktools.model.video.MyVideo;
import com.opencosy.networktools.model.youtube.VideoSnippetHandler;
import com.opencosy.networktools.utils.CSV;
import com.opencosy.networktools.utils.FileHelp;
import com.opencosy.networktools.utils.GraphView;
import com.opencosy.networktools.utils.TableUtils;
import com.opencosy.networktools.utils.metrics.LocalClusteringCoefficientMetric;
import com.opencosy.networktools.utils.metrics.LocalClusteringCoefficientResult;
import com.opencosy.networktools.utils.process.Converter;
import com.opencosy.networktools.utils.process.Download;
import com.opencosy.networktools.utils.process.FeatureExtraction;
import com.opencosy.networktools.utils.process.Similarity;
import com.opencosy.networktools.utils.wav.WavFile;
import com.opencosy.networktools.utils.wav.WavFileException;
import com.opencosy.networktools.view.activelearning.ActiveLearning;
import com.opencosy.networktools.view.add.AddVideo;
import com.opencosy.networktools.view.add.AddVideoController;
import com.opencosy.networktools.view.create.Create;
import com.opencosy.networktools.view.segmentation.Segmentation;
import com.opencosy.networktools.view.start.Start;
import com.opencsv.CSVReader;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.*;

import static com.opencosy.networktools.utils.Constants.*;

public class MainController implements Initializable, Observer {

    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    /**
     * Variable for multiply choose of checkboxs
     */
    public static boolean singleItem = true;

    @FXML
    private Button run;

    @FXML
    private Button segmentButton;

    @FXML
    private MenuItem activeLearningItem;


    /**
     * database object
     */
    private DBConnector connector = new DBConnector();
    /**
     * List of active download
     */
    private Map<Download, Thread> listDownloads = new HashMap<>();
    /**
     * Youtube model
     */
    private VideoSnippetHandler model = new VideoSnippetHandler();
    private List<MyVideo> selectedList = new LinkedList<>();

    @FXML
    private CheckMenuItem hideId;
    @FXML
    private CheckMenuItem hideDownload;
    @FXML
    private CheckMenuItem hideConvert;
    @FXML
    private MenuBar menuBar;
    @FXML
    private MenuItem newItem;
    @FXML
    private MenuItem openItem;
    @FXML
    private MenuItem loadFiles;
    @FXML
    private MenuItem loadDirectories;
    @FXML
    private MenuItem loadTXT;
    @FXML
    private MenuItem exitItem;
    @FXML
    private MenuItem selectItem;
    @FXML
    private MenuItem unselectItem;
    @FXML
    private MenuItem revertItem;
    @FXML
    private MenuItem refreshDataItem;
    @FXML
    private MenuItem buildGraphItem;
    @FXML
    private MenuItem segmentationItem;
    @FXML
    private MenuItem addItem;
    @FXML
    private MenuItem iterationItem;
    @FXML
    private MenuItem downloadItem;
    @FXML
    private MenuItem convertItem;
    @FXML
    private MenuItem deleteIdItem;
    @FXML
    private MenuItem deleteVideoItem;
    @FXML
    private MenuItem logItem;
    @FXML
    private MenuItem configItem;
    @FXML
    private MenuItem aboutItem;

    @FXML
    private TableView<MyVideo> videoTable;

    @FXML
    private TableColumn<MyVideo, Number> idColumn;
    @FXML
    private TableColumn<MyVideo, Boolean> checkboxColumn;
    @FXML
    private TableColumn<MyVideo, String> videoIdColumn;


    @FXML
    private TableColumn<MyVideo, String> downloadColumn;
    @FXML
    private TableColumn<MyVideo, String> convertColumn;

    @FXML
    private TableColumn<MyVideo, String> rawLldARFFColumn;


    @FXML
    private TableColumn<MyVideo, Double> similarColumn;

    @FXML
    private TableColumn<MyVideo, Double> lccColumn;

    @FXML
    private Button lccButton;


    @FXML
    private Button similarButton;

    @FXML
    private Button getLLD;

    /**
     * Context menu for TableView
     */
    private ContextMenu cm = new ContextMenu();

    public static Segment segment = new Segment(1, "30");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addMouseListenerToTable();
        addActionListener();
        initMenuItems();
        initTable();
    }

    private void initTable() {
        checkboxColumn.setEditable(true);
        videoTable.setEditable(true);
        videoTable.getSelectionModel().setCellSelectionEnabled(true);
        videoTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        idColumn.setCellValueFactory(new PropertyValueFactory<>("index"));
        videoIdColumn.setCellValueFactory(new PropertyValueFactory<>("videoId"));
        downloadColumn.setCellValueFactory(new PropertyValueFactory<>("downloadedVideo"));
        convertColumn.setCellValueFactory(new PropertyValueFactory<>("convertedVideo"));
        rawLldARFFColumn.setCellValueFactory(new PropertyValueFactory<>("lldVideo"));

        similarColumn.setCellValueFactory(new PropertyValueFactory<>("similar"));
        checkboxColumn.setCellValueFactory(p -> p.getValue().isSelectedProperty());
        checkboxColumn.setCellFactory(p -> new CheckBoxTableCell<>());
        lccColumn.setCellValueFactory(new PropertyValueFactory<>("lcc"));

        videoTable.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
            if (t.getButton() == MouseButton.SECONDARY) {
                cm.hide();
                cm = new ContextMenu();
                createContextMenu(cm);
                videoTable.setContextMenu(cm);
                cm.show(videoTable, t.getScreenX(), t.getScreenY());
            }
        });

        TableUtils.installCopyPasteHandler(videoTable, connector);
        buildData(true);
    }

    private void createContextMenu(ContextMenu cm) {
        TablePosition pos = videoTable.getSelectionModel().getSelectedCells().get(0);
        String column = pos.getTableColumn().getId();
        int row = pos.getRow();

        switch (column) {
            case "index":
                break;
            case "checkboxColumn":
                break;
            case "videoId":
                break;
            case "downloadColumn":
                selectDownload(cm, row);
                break;
            case "convertColumn":
                break;
        }
    }

    private void selectDownload(ContextMenu cm, int row) {
        if (listDownloads != null && listDownloads.size() > 0) {
            MenuItem item = new MenuItem("Stop download");
            cm.getItems().add(item);
            item.setOnAction(event -> {
                MyVideo video = videoTable.getItems().get(row);
                stopDownload(video);
            });
        }
    }


    /**
     * Set on click function for menu items
     */
    private void initMenuItems() {
        openItemInit();
        newItemInit();
        exitItemInit();
        loadTXTItem();
        loadDirectoriesItem();
        loadFilesItem();
        configItemInit();
        selectItemInit();
        unselectItemInit();
        revertItemInit();
        addVideoItemInit();
        buildGraphItemInit();
        segmentationItemInit();
        activeLearningItemInit();
        iterationItemInit();
        downloadItemInit();
        convertItemInit();
        deleteVideoItemInit();
        deleteIdItemInit();
        featuresExtractionItemInit();
        logItemInit();
        refreshDataItemInit();
        aboutItemInit();
    }

    private void activeLearningItemInit() {
        activeLearningItem.setOnAction(event -> new ActiveLearning());
    }

    private void segmentationItemInit() {
        segmentationItem.setOnAction(event -> {
            new Segmentation();
        });
    }

    /**
     * Open files or link to video with mouse double click
     * Set on click handler from keyboard
     */
    private void addMouseListenerToTable() {
        videoTable.setOnMouseClicked(click -> {
            if (click.getClickCount() == 2) {
                TableUtils.openItem(videoTable);
            }
        });
    }


    /**
     * load data from database
     */
    private void buildData(boolean changeListener) {
        Platform.runLater(() -> {
            //connect to database for getting videos id, nodes and edges
            if (listDownloads.size() == 0) {
                connector.connect(DATABASE_PATH, false);
                List<MyVideo> videos = connector.readVideoId();
                List<Integer> nodes = connector.readNode();
                List<Edge> edges = connector.readEdge();
                connector.closeDB();

                // add data to table
                videoTable.setItems(makeData(nodes, edges, videos));
                videoTable.refresh();

                //add listeners to checkbox
                if (changeListener) {
                    setActionListenerToCheckbox();
                }
                //Set title for window
                setWindowTitle();
            }
        });
    }

    //File tab //
    private void newItemInit() {
        newItem.setOnAction(event -> {
                    FileChooser chooser = new FileChooser();
                    chooser.setTitle("New database");
                    File file = chooser.showSaveDialog(new Stage());
                    if (file != null) {
                        synchronized (this) {
                            DATABASE_PATH = file.getPath();
                        }
                        connector.connect(DATABASE_PATH, true);
                        //  connector.newDatabase();
                        connector.closeDB();
                        Property.setProperties(DATABASE_PATH_TEXT, DATABASE_PATH);
                        Stage stage = (Stage) menuBar.getScene().getWindow();
                        stage.hide();
                    }
                }
        );
    }

    private void openItemInit() {
        openItem.setOnAction(event -> {
                    FileChooser chooser = new FileChooser();
                    chooser.setTitle("Open File");
                    File file = chooser.showOpenDialog(new Stage());
                    if (file == null) {
                        return;
                    }
                    synchronized (this) {
                        DATABASE_PATH = file.getPath();
                        DATA_PATH = file.getParent();
                    }
                    Property.setProperties(DATABASE_PATH_TEXT, DATABASE_PATH);
                    Property.setProperties(DATA_PATH_TEXT, DATA_PATH);
                    buildData(true);

                }
        );
    }

    private void loadTXTItem() {
        loadTXT.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open File");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                try {
                    CSVReader reader = new CSVReader(new FileReader(file.getPath()), '\t');
                    String[] nextLine;
                    connector.connect(DATABASE_PATH, false);
                    while ((nextLine = reader.readNext()) != null) {
                        Video video = model.getVideo(nextLine[0]);
                        if (video != null) {
                            connector.writeVideoToDB(video, null, 1, 0);
                        }
                    }
                    connector.closeDB();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                buildData(true);
            }
        });
    }

    private void loadDirectoriesItem() {
        loadDirectories.setOnAction(event -> {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Open directory");
            File file = chooser.showDialog(new Stage());
            if (file != null) {
                File[] dir = file.listFiles();
                if (dir != null) {
                    connector.connect(DATABASE_PATH, false);
                    for (File id : dir) {
                        Video video = model.getVideo(id.getName());
                        if (video != null) {
                            connector.writeVideoToDB(video, null, 1, 0);
                        }
                    }
                    connector.closeDB();
                    buildData(true);
                }
            }
        });
    }

    private void loadFilesItem() {
        loadFiles.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open files");
            List<File> files = chooser.showOpenMultipleDialog(new Stage());
            if (files != null) {
                connector.connect(DATABASE_PATH, false);
                for (File id : files) {
                    String name = id.getName().replace("." + Files.getFileExtension(id.getName()), "");
                    Video video = model.getVideo(name);
                    if (video != null) {
                        connector.writeVideoToDB(video, null, 1, 0);
                    }
                }
                connector.closeDB();
                buildData(true);
            }
        });
    }


    private void exitItemInit() {
        exitItem.setOnAction(event -> System.exit(0));
    }

    //Edit tab //

    private void revertItemInit() {
        revertItem.setOnAction(event -> {
            synchronized (this) {
                singleItem = false;
            }
            TableUtils.revertAllCheckbox(videoTable);
            writeCheckboxToDB();
        });
    }

    private void unselectItemInit() {
        unselectItem.setOnAction(event -> {
            synchronized (this) {
                singleItem = false;
            }
            TableUtils.unselectAllCheckbox(videoTable);
            writeCheckboxToDB();
        });
    }

    private void selectItemInit() {
        selectItem.setOnAction(event -> {
            synchronized (this) {
                singleItem = false;
            }
            TableUtils.selectAllCheckbox(videoTable);
            writeCheckboxToDB();
        });
    }

    // Data tab //

    /**
     * rebuild table
     */
    private void refreshDataItemInit() {
        refreshDataItem.setOnAction(event -> buildData(true));
    }

    /**
     * build graph
     */
    private void buildGraphItemInit() {
        buildGraphItem.setOnAction(event -> {
            connector.connect(DATABASE_PATH, false);
            List<Integer> nodes = connector.readNode();
            List<Edge> edges = connector.readEdge();
            connector.closeDB();
            List<String> lcc = new LinkedList<>();
            for (MyVideo video : videoTable.getItems()) {
                lcc.add(video.getLcc());
            }
            System.out.println(lcc.size());
            new CSV().writeToFile(nodes, edges);
            GraphView graph = new GraphView(nodes, edges, lcc);
            graph.paintGraph();
        });
    }

    private void featuresExtractionItemInit() {
        // featuresExtractionItem.setOnAction(event -> new ExtractionView());
    }

    private void addVideoItemInit() {
        addItem.setOnAction(event -> {
            AddVideo addVideoView = new AddVideo();
            addVideoView.getController().addObserver(MainController.this);
        });
    }

    private void iterationItemInit() {
        iterationItem.setOnAction(event -> {
            connector.connect(DATABASE_PATH, false);
            List<String> listYoutubeKey = connector.readVideoTitle();
            connector.closeDB();
            System.out.println("related list: " + listYoutubeKey.size());
            connector.connect(DATABASE_PATH, false);
            for (int i = 0; i < videoTable.getItems().size(); i++) {
//                for (String id : listYoutubeKey) {
                if (videoTable.getItems().get(i).getSelected()) {
                    //      new Thread(() -> {

                    Video video = model.getVideo(videoTable.getItems().get(i).getVideoId());
                    if (video != null) {
                        long time = System.currentTimeMillis();
                        List<Video> relatedVideos = model.getRelatedVideo(video.getId(), Long.valueOf(RELATEDVIDEO_COUNT));
                        connector.writeVideoToDB(video, relatedVideos, 1, 0);

                        System.out.println("Write Video: " + (System.currentTimeMillis() - time));
                        if (COMMENTS) {
                            List<Comment> comments = model.getComments(video.getId());
                            long time2 = System.currentTimeMillis();
                            connector.writeCommentToDBAll(comments, video.getId());
                            System.out.println("Write comment: " + (System.currentTimeMillis() - time2));
                        }
                    }

                    buildData(true);
                    //    }).start();
                }
            }
//            }
            connector.closeDB();
        });

    }

    private void downloadItemInit() {
        downloadItem.setOnAction(event -> download());
    }

    private void convertItemInit() {
        convertItem.setDisable(!FileHelp.checkConverter());
        convertItem.setOnAction(event -> convert());
    }

    private void deleteVideoItemInit() {
        deleteVideoItem.setOnAction(event -> {
                    for (int i = 0; i < videoTable.getItems().size(); i++) {
                        if (videoTable.getItems().get(i).getSelected()) {
                            String path = videoTable.getItems().get(i).getDownloadedVideo();
                            if (path == null) {
                                break;
                            }
                            File file = new File(path);
                            if (!file.exists()) {
                                break;
                            }
                            if (file.delete()) {
                                videoTable.getItems().get(i).setDownloadedVideo("-");
                            }
                        }
                    }
                    videoTable.refresh();
                }
        );
    }

    private void deleteIdItemInit() {
        deleteIdItem.setOnAction(event -> {
                    List<String> listYoutubeKey = new LinkedList<>();
                    for (int i = 0; i < videoTable.getItems().size(); i++) {
                        if (videoTable.getItems().get(i).getSelected()) {
                            listYoutubeKey.add(videoTable.getItems().get(i).getVideoId());
                        }
                    }

                    connector.connect(DATABASE_PATH, false);
                    List<Edge> edges = connector.readEdge();
                    for (String id : listYoutubeKey) {
                        int videoId = connector.getVideoID(id, 1);
                        int counter = 0;
                        for (Edge edge : edges) {
                            if (edge.getStart() == videoId) {
                                counter++;
                            }
                        }
                        if (counter <= 2) {
                            connector.changeStatus(id, 0);
                        }
                    }
                    connector.closeDB();
                    buildData(false);
                }
        );
    }


    // Help tab
    private void configItemInit() {
        configItem.setOnAction(event -> {
            Stage stage = (Stage) videoTable.getScene().getWindow();
            stage.hide();
            FIRST_START = false;
            new Start().start(new Stage());
        });
    }

    private void aboutItemInit() {
        aboutItem.setOnAction(event -> {
            try {
                Stage stage = new Stage();
                String fxmlFile = "/fxml/about.fxml";
                FXMLLoader loader = new FXMLLoader();

                loader.setLocation(Create.class.getResource(fxmlFile));
                AnchorPane rootLayout = loader.load();

                // Show the scene containing the root layout.
                Scene scene = new Scene(rootLayout);
                stage.setResizable(false);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                logger.error("Can't load \"About\" window", e);
            }
        });
    }

    private void logItemInit() {
        // logItem.setOnAction(event -> new LogView());
    }

    // Help methods
    private void download() {
        for (int i = 0; i < videoTable.getItems().size(); i++) {
            if (videoTable.getItems().get(i).getSelected()) {
                int finalI = i;
                MyVideo video = videoTable.getItems().get(finalI);
                if (video.getDownloadedVideo().equals("-")) {
                    Download download = new Download();
                    download.addObserver(MainController.this);
                    download.setVideo(video);

                    checkDownloadsSize();
                    download.setId(video.getVideoId());

                    Thread thread = new Thread(download);
                    listDownloads.put(download, thread);
                    thread.start();
                }
            }
            videoTable.refresh();
        }
    }

//    private void startDownload(MyVideo video) {
//
//    }

    private void convert() {
        for (int i = 0; i < videoTable.getItems().size(); i++) {
            if (checkConvertCell(videoTable.getItems().get(i)) && videoTable.getItems().get(i).getSelected()) {
                int finalI = i;
                new Thread(() -> {
                    try {
                        Converter converter = new Converter();
                        converter.addObserver(MainController.this);
                        converter.setVideo(videoTable.getItems().get(finalI));
                        converter.convert(videoTable.getItems().get(finalI).getDownloadedVideo());
                    } catch (IOException e) {
                        logger.error("Can't convert audio", e);
                    }
                }).start();
            }

        }
        videoTable.refresh();
    }

    private void downloadConvert() {
        for (MyVideo video : selectedList) {
            if (checkDownloadCell(video)) {
                new Thread(() -> {
                    if (videoTable.getItems().indexOf(video) != -1) {
                        //startDownload(videoTable.getItems().get(videoTable.getItems().indexOf(video)));
                        convert();
                    }
                }).start();
            }
            videoTable.refresh();
        }
    }

    private void checkDownloadsSize() {
//        if (listDownloads.size() == 0) {
//            stopButton.setDisable(true);
//        } else {
//            stopButton.setDisable(false);
//        }
    }

    private void writeCheckboxToDB() {
        Map<String, Integer> map = new HashMap<>();
        // selectedList = new LinkedList<>();
        connector.connect(DATABASE_PATH, false);
        for (int i = 0; i < videoTable.getItems().size(); i++) {
            MyVideo video = videoTable.getItems().get(i);
            map.put(video.getVideoId(), video.getSelected() ? 1 : 0);
            setSelectedList(selectedList, video);
        }
        connector.changeSelectes(map);
        connector.closeDB();
        synchronized (this) {
            singleItem = true;
        }
    }

    private void updateLabels(int node, int edge) {
//        videoLabel.setText(String.valueOf(node));
//        relatedVdieo.setText(String.valueOf(edge));
    }

    private ObservableList<MyVideo> makeData(List<Integer> nodes, List<Edge> edges,
                                             List<MyVideo> videos) {
        ObservableList<MyVideo> data = FXCollections.observableArrayList();
        GraphView graph = new GraphView(nodes, edges, null);
        LocalClusteringCoefficientMetric<String, DefaultEdge> local = new LocalClusteringCoefficientMetric<>(graph.getGraph());
        LocalClusteringCoefficientResult<String> result = local.calculate();

        for (MyVideo video : videos) {
            String s = String.format("%.2f", local.calculateVertex(String.valueOf(video.getIndex())));
            video.setLcc(s);
            data.add(video);
        }

        System.out.println(result.getAverageClusteringCoefficient());
        return data;
    }

    private void setActionListenerToCheckbox() {
        for (int i = 0; i < videoTable.getItems().size(); i++) {
            MyVideo video = videoTable.getItems().get(i);
            video.isSelectedProperty().addListener((observable, oldValue, newValue) -> {
                if (singleItem) {
                    connector.connect(DATABASE_PATH, false);
                    connector.changeSelect(video.getVideoId(), newValue ? 1 : 0);
                    connector.closeDB();
                    setSelectedList(selectedList, video);
                }
            });
            setSelectedList(selectedList, video);
        }
    }

    private boolean checkDownloadCell(MyVideo video) {
        return (!Objects.equals(video.getDownloadedVideo(), "-")
                || !Objects.equals(video.getDownloadedVideo(), null) &&
                !video.getDownloadedVideo().contains("DOWNLOADING"));
    }

    private boolean checkConvertCell(MyVideo video) {
        return (Objects.equals(video.getConvertedVideo(), "-") ||
                Objects.equals(video.getConvertedVideo(), null)) &&
                checkDownloadCell(video);
    }

    private void stopDownload(MyVideo video) {
        if (listDownloads != null) {
            for (Download download : listDownloads.keySet()) {
                if (download.getVideo().getVideoId().equals(video.getVideoId())) {
                    download.setStop();
                    download.setStoped(true);
//                    listDownloads.get(download).interrupt();
//                    listDownloads.get(download).notifyAll();
                    // listDownloads.remove(download);
                    refreshPath(download.getVideo());
                }
                // checkDownloadsSize();
            }
        }
    }

    private void setSelectedList(List<MyVideo> selectedList, MyVideo video) {
        if (video.getSelected()) {
            selectedList.add(video);
        } else {
            selectedList.remove(video);
        }
    }

    private void deleteVideo(String path) {
        File downloadFile = new File(path);
        File dir = new File(downloadFile.getParent());
        downloadFile.delete();
        dir.delete();
        videoTable.refresh();
    }

    private void refreshPath(MyVideo video) {
        String download = FileHelp.checkFile(video.getVideoId(), 1);
        String convert = FileHelp.checkFile(video.getVideoId(), 2);
        video.setDownloadedVideo(download);
        video.setConvertedVideo(convert);
        videoTable.refresh();
    }

    private void setWindowTitle() {
        Stage stage = (Stage) videoTable.getScene().getWindow();
        stage.setTitle(DATABASE_PATH);
    }

    @Override
    public void update(Observable o, Object arg) {
        String outputPath;
        if (o instanceof Download) {
            outputPath = ((Download) o).getFinish();
            if (videoTable.getItems().indexOf(((Download) o).getVideo()) != -1) {
                MyVideo video = videoTable.getItems().get(videoTable.getItems().indexOf(((Download) o).getVideo()));
                video.setDownloadedVideo(outputPath);
            }
            if (((Download) o).isStoped()) {
                //  deleteVideo(outputPath);
                refreshPath(((Download) o).getVideo());
            }

            // checkDownloadsSize();
            videoTable.refresh();
        }
        if (o instanceof Converter) {
            outputPath = ((Converter) o).getFinish();
            System.out.println(outputPath);
            if (videoTable.getItems().indexOf(((Converter) o).getVideo()) != -1) {
                MyVideo video2 = videoTable.getItems().get(videoTable.getItems().indexOf(((Converter) o).getVideo()));
                video2.setConvertedVideo(outputPath);

            }
            if (((Converter) o).getStopped()) {
                refreshPath(((Converter) o).getVideo());
            }
        }
        if (o instanceof AddVideoController) {
            boolean finish = ((AddVideoController) o).getFinish();
            if (finish) {
                buildData(true);
            }
        }
//        if (o instanceof ConfigController) {
//            boolean finish = ((ConfigController) o).getFinish();
//            if (finish) {
//                convertItem.setDisable(!FileHelp.checkConverter());
//                buildData(true);
//            }
//        }
    }

    private void addActionListener() {
        hideId.setOnAction(event -> {
            idColumn.setVisible(!hideId.isSelected());
        });

        hideDownload.setOnAction(event -> {
            downloadColumn.setVisible(!hideDownload.isSelected());
        });

        hideConvert.setOnAction(event -> {
            convertColumn.setVisible(!hideConvert.isSelected());
        });

        lccButton.setOnAction(event -> {
            for (int i = 0; i < videoTable.getItems().size(); i++) {
                String llc = videoTable.getItems().get(i).getLcc();
                llc = llc.replace(",", ".");
                if (Double.valueOf(llc) < 0.001) {
                    videoTable.getItems().get(i).setSelected(true);
                }
            }
        });

        getLLD.setOnAction(event -> {
            for (int i = 0; i < videoTable.getItems().size(); i++) {
                if (!videoTable.getItems().get(i).getConvertedVideo().equals("-") && videoTable.getItems().get(i).getSelected()) {
                    String lld = FeatureExtraction.processARFF(videoTable.getItems().get(i).getConvertedVideo(), true);
                    videoTable.getItems().get(i).setLldVideo(lld);
                }
            }
        });

        similarButton.setOnAction(event -> {
            for (int i = 0; i < videoTable.getItems().size(); i++) {
                if (!videoTable.getItems().get(i).getConvertedVideo().equals("-") && videoTable.getItems().get(i).getSelected()) {
                    try {
                        String content = IOUtils.toString(new FileInputStream(videoTable.getItems().get(i).getArff()), "UTF-8");
                        content = content.replaceAll("@attribute class numeric", "@attribute class {1,2,3,4,5,6}");
                        IOUtils.write(content, new FileOutputStream(videoTable.getItems().get(i).getArff()), "UTF-8");
                    } catch (IOException e) {
                        logger.error("Can't find similarity ", e);
                    }
                }
            }
            for (int i = 0; i < videoTable.getItems().size(); i++) {
                if (!videoTable.getItems().get(i).getArff().equals("-") && videoTable.getItems().get(i).getSelected()) {
                    try {
                        LinkedList<Double> train = Similarity.processSimilarity(TRAIN_PATH, "6");
                        //    LinkedList<Double> devel = Similarity.processSimilarity(videoTable.getItems().get(i).getArff(), "1");
                        LinkedList<Double> devel = Similarity.processSimilarity("/Users/strange/Documents/Bachelor/YT/arff/1sample.is10.devel.arff", "2");
                        double r = 0.0;
                        for (int k = 0; k < 76; k++) {
                            r = r + train.get(k) * devel.get(k);
                        }
                        videoTable.getItems().get(i).setSimilar(r / (76 - 1.0));
                    } catch (Exception e) {
                        logger.error("TEST", e);
                    }
                }
            }
        });

        segmentButton.setOnAction(event -> {
            int mode = segment.getMode();
            switch (mode) {
                case 0:
                    for (int i = 0; i < videoTable.getItems().size(); i++) {
                        if (!videoTable.getItems().get(i).getConvertedVideo().equals("-") && videoTable.getItems().get(i).getSelected()) {
                            try {
                                WavFile wavFile = WavFile.openWavFile(new File(videoTable.getItems().get(i).getConvertedVideo()));
                                long sampleRate = wavFile.getSampleRate();
                                long frames = wavFile.getNumFrames();
                                double time = frames / sampleRate;

                                int start = Integer.valueOf(segment.getStart());
                                int end = Integer.valueOf(segment.getEnd());
                                if (start > 0 && (end - start) > 0 && (time - end) >= 0) {
                                    WavFile.segmentWave(videoTable.getItems().get(i).getConvertedVideo(), start, end);
                                } else {
                                    int temp1 = end - start;
                                    double temp2 = time - end;
                                    System.out.println(start + "  " + temp1 + "   " + temp2);
                                }
                            } catch (IOException | WavFileException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    break;
                case 1:
                    for (int i = 0; i < videoTable.getItems().size(); i++) {
                        if (!videoTable.getItems().get(i).getConvertedVideo().equals("-") && videoTable.getItems().get(i).getSelected()) {
                            try {
                                WavFile wavFile = WavFile.openWavFile(new File(videoTable.getItems().get(i).getConvertedVideo()));
                                long sampleRate = wavFile.getSampleRate();
                                long frames = wavFile.getNumFrames();
                                double time = frames / sampleRate;
                                int period = Integer.valueOf(segment.getPeriod());
                                int start = 0;
                                int end = period;
                                while (time >= period) {
                                    WavFile.segmentWave(videoTable.getItems().get(i).getConvertedVideo(), start, end);
                                    start = end;
                                    end = end + period;
                                    time = time - period;
                                }
                                WavFile.segmentWave(videoTable.getItems().get(i).getConvertedVideo(), start, (int) (frames / sampleRate));
                            } catch (IOException | WavFileException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
                case 2:
                    break;
            }
        });
    }
}
