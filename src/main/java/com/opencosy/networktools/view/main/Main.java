package com.opencosy.networktools.view.main;


import com.opencosy.networktools.utils.Constants;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Load StartController
 */
public class Main {

    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * Constructor of class
     */
    public Main() {
        start();
    }

    /**
     * Load Main window
     */
    private void start() {
        try {
            Stage stage = new Stage();
            String fxmlFile = "/fxml/main.fxml";
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource(fxmlFile));
            AnchorPane rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            stage.setTitle(Constants.DATABASE_PATH);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            logger.error("Can't load \"Main\" window", e);
        }
    }
}
