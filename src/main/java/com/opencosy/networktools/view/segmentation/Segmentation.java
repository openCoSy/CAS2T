package com.opencosy.networktools.view.segmentation;


import com.opencosy.networktools.view.create.Create;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Segmentation {
    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(Create.class);


    /**
     * Constructor for create database
     */
    public Segmentation() {
        open();
    }

    /**
     * Load resources and open window
     */
    private void open() {
        try {
            Stage stage = new Stage();
            String fxmlFile = "/fxml/cut.fxml";
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Create.class.getResource(fxmlFile));
            AnchorPane rootLayout = loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            stage.setResizable(false);
            stage.setTitle("File segmentation");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            logger.error("Can't load \"Segmentation\" window", e);
        }
    }
}
