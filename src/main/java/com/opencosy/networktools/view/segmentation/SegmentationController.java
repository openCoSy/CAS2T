package com.opencosy.networktools.view.segmentation;


import com.opencosy.networktools.model.audio.Segment;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

import static com.opencosy.networktools.view.main.MainController.segment;

public class SegmentationController implements Initializable {
    @FXML
    private RadioButton intervalRadio;
    @FXML
    private RadioButton periodRadio;
    @FXML
    private RadioButton AEDRadio;
    @FXML
    private TextField startTime;
    @FXML
    private TextField endTime;
    @FXML
    private TextField period;
    @FXML
    private Button startButton;

    // private Segment segment;

    private int mode = 0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        final ToggleGroup group = new ToggleGroup();

        intervalRadio.setToggleGroup(group);
        intervalRadio.setSelected(true);
        periodRadio.setToggleGroup(group);
        AEDRadio.setToggleGroup(group);

        group.selectedToggleProperty().addListener((ov, t, t1) -> {
            RadioButton chk = (RadioButton) t1.getToggleGroup().getSelectedToggle(); // Cast object to radio button
            switch (chk.getText()) {
                case "Interval":
                    mode = 0;
                    break;
                case "Period":
                    mode = 1;
                    break;
                case "AED":
                    mode = 2;
                    break;
            }
        });

        startButton.setOnAction(event -> {
            switch (mode) {
                case 0:
                    segment = new Segment(0, startTime.getText(), endTime.getText());
                    break;
                case 1:
                    segment = new Segment(1, period.getText());
                    break;
                case 2:
                    segment = new Segment(2);
                    break;
            }

            Stage stage = (Stage) startButton.getScene().getWindow();
            stage.close();
        });
    }
}
