package com.opencosy.networktools.view.create;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.opencosy.networktools.utils.Constants.DATABASE_PATH;

/**
 * Create new database
 */
public class Create {
    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(Create.class);

    /**
     * Constructor for create database
     */
    public Create() {
        open();
    }

    /**
     * Load resources and open window
     */
    private void open() {
        try {
            Stage stage = new Stage();
            String fxmlFile = "/fxml/newdb.fxml";
            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(Create.class.getResource(fxmlFile));
            AnchorPane rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            stage.setResizable(false);
            stage.setTitle(DATABASE_PATH);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            logger.error("Can't load \"Create\" window", e);
        }
    }
}
