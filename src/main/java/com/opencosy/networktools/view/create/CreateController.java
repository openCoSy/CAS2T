package com.opencosy.networktools.view.create;


import com.google.api.services.youtube.model.Comment;
import com.google.api.services.youtube.model.Video;
import com.opencosy.networktools.model.database.DBConnector;
import com.opencosy.networktools.model.property.Property;
import com.opencosy.networktools.model.youtube.VideoSnippetHandler;
import com.opencosy.networktools.view.main.Main;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static com.opencosy.networktools.utils.Constants.*;

/**
 * Controller for creating new database
 */
public class CreateController implements Initializable {

    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(CreateController.class);

    @FXML
    private Button startButton;
    @FXML
    private CheckBox commentCheckbox;
    @FXML
    private TextField urlField;

    @Override
    public synchronized void initialize(URL location, ResourceBundle resources) {
        COMMENTS = false;
        //commentCheckbox.isSelected();
        startButton.setOnAction(event -> {
            String id = urlField.getText();
            id = id.replace(PREFIX, "");
            DBConnector connector = new DBConnector();
            VideoSnippetHandler model = new VideoSnippetHandler();
            DATA_PATH = new File(DATABASE_PATH).getParent();
            Property.setProperties(DATA_PATH_TEXT, DATA_PATH);
            connector.connect(DATABASE_PATH, false);
            getVideo(id, model, connector);
            getComments(id, model, connector);
            connector.closeDB();
            loadMainWindow();
        });
    }


    private void loadMainWindow() {
        Stage stage = (Stage) startButton.getScene().getWindow();
        stage.close();
        new Main();
    }

    private void getVideo(String id, VideoSnippetHandler model, DBConnector connector) {
        Video video = model.getVideo(id);
        List<Video> relatedVideos = model.getRelatedVideo(id, Long.valueOf(RELATEDVIDEO_COUNT));
        if (video != null) {
            connector.writeVideoToDB(video, relatedVideos, 1, 0);
        }
    }

    private void getComments(String id, VideoSnippetHandler model, DBConnector connector) {
        if (COMMENTS) {
            List<Comment> comments = model.getComments(id);
            for (Comment comment : comments) {
                connector.writeCommentToDB(comment);
            }
        }
    }
}
