package com.opencosy.networktools.view.add;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by strange on 09/02/2017.
 */
public class AddVideo {
    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(AddVideo.class);
    private AddVideoController controller;
    /**
     * Constructor for create database
     */
    public AddVideo() {
        open();
    }

    /**
     * Load resources and open window
     */
    private void open() {
        try {
            Stage stage = new Stage();
            String fxmlFile = "/fxml/addVideo.fxml";
            FXMLLoader fxmlLoader = new FXMLLoader(AddVideo.class.getResource(fxmlFile));
            AnchorPane rootLayout = fxmlLoader.load();
            controller = fxmlLoader.getController();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            stage.setResizable(false);
            stage.setTitle("Add video");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            logger.error("Can't load \"Add\" window", e);
        }
    }
    public AddVideoController getController() {
        return controller;
    }

    public void setController(AddVideoController controller) {
        this.controller = controller;
    }
}
