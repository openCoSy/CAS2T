package com.opencosy.networktools.view.add;


import com.google.api.services.youtube.model.Video;
import com.opencosy.networktools.model.database.DBConnector;
import com.opencosy.networktools.model.youtube.VideoSnippetHandler;
import com.opencosy.networktools.utils.Constants;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;

public class AddVideoController extends Observable implements Initializable {
    @FXML
    private TextField videoIdTextField;
    @FXML
    private Button addButton;
    @FXML
    private CheckBox relatedCheckbox;

    private boolean finish = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addButton.setOnAction(event -> {
            String id = videoIdTextField.getText();
            id = id.replace(Constants.PREFIX, "");
            DBConnector connector = new DBConnector();
            connector.connect(Constants.DATABASE_PATH, false);

            VideoSnippetHandler model = new VideoSnippetHandler();
            Video video = model.getVideo(id);
            boolean isSelected = relatedCheckbox.isSelected();
            if (isSelected) {
                List<Video> relatedVideo = model.getRelatedVideo(id, Long.valueOf(Constants.RELATEDVIDEO_COUNT));
                connector.writeVideoToDB(video, relatedVideo, 1, 0);
            } else{
                connector.writeVideoToDB(video, null, 1, 0);
            }
            connector.closeDB();
            Stage stage = (Stage) addButton.getScene().getWindow();
            stage.hide();
            setFinish(true);
        });

    }

    public void setFinish(Boolean finish) {
        synchronized (this) {
            this.finish = finish;
        }
        setChanged();
        notifyObservers();
    }

    public synchronized boolean getFinish() {
        return finish;
    }
}