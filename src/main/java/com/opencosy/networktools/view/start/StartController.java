package com.opencosy.networktools.view.start;

import com.opencosy.networktools.view.main.Main;
import com.opencosy.networktools.model.property.Property;
import com.opencosy.networktools.utils.FileHelp;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import static com.opencosy.networktools.model.property.Property.getProperties;
import static com.opencosy.networktools.model.property.Property.setProperties;
import static com.opencosy.networktools.utils.ConfigValidation.*;
import static com.opencosy.networktools.utils.Constants.*;

/**
 * Controller for start window
 */
public class StartController implements Initializable {

    /**
     * TableView with properties
     */
    @FXML
    private TableView<Property> configTable;

    /**
     * Column with property's name
     */
    @FXML
    private TableColumn<Property, String> key;

    /**
     * Column with property's value
     */
    @FXML
    private TableColumn<Property, String> value;

    /**
     * Column with property's status
     */
    @FXML
    private TableColumn<Property, ImageView> status;

    /**
     * Start button
     * Active if all parameters are correct
     * Close window and open main window
     */
    @FXML
    private Button startButton;

    boolean isError = false;

    /**
     * Context menu for TableView
     */
    private ContextMenu cm = new ContextMenu();

    /**
     * Log variable
     */
    private static final Logger logger = LoggerFactory.getLogger(StartController.class);

    /**
     * Init view
     *
     * @param location  1
     * @param resources 2
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initTable();
        startButton.setDisable(true);
        addActionListener();
        buildData();
    }

    /**
     * Set parameter to table
     */
    private void initTable() {
        value.setEditable(true);
        configTable.setEditable(true);
        configTable.getSelectionModel().setCellSelectionEnabled(true);
        configTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        key.setCellValueFactory(new PropertyValueFactory<>("key"));
        value.setCellFactory(TextFieldTableCell.forTableColumn());
        value.setCellValueFactory(new PropertyValueFactory<>("value"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));
        configTable.setColumnResizePolicy((param) -> true);

        configTable.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
            if (t.getButton() == MouseButton.SECONDARY) {
                cm.hide();
                cm = new ContextMenu();
                createContextMenu(cm);
                configTable.setContextMenu(cm);
                cm.show(configTable, t.getScreenX(), t.getScreenY());
            }
        });
    }

    /**
     * add action listener to button and value column
     */
    private void addActionListener() {
        startButton.setOnAction(event -> {
            Stage stage = (Stage) configTable.getScene().getWindow();
            stage.hide();
            if (!FileHelp.checkDirectories()) {
                logger.error("Can't create folders");
            }
            new Main();
        });

        value.setOnEditCommit(event -> {
            switch (event.getRowValue().getKey()) {
                case RELATEDVIDEO_COUNT_TEXT:
                    synchronized (this) {
                        RELATEDVIDEO_COUNT = event.getNewValue();
                    }
                    setProperties(RELATEDVIDEO_COUNT_TEXT, RELATEDVIDEO_COUNT);
                    break;
                case DATABASE_PATH_TEXT:
                    synchronized (this) {
                        DATABASE_PATH = event.getNewValue();
                    }
                    setProperties(DATABASE_PATH_TEXT, DATABASE_PATH);
                    break;
                case DATA_PATH_TEXT:
                    synchronized (this) {
                        DATA_PATH = event.getNewValue();
                    }
                    setProperties(DATA_PATH_TEXT, DATA_PATH);
                    break;
                case PROPERTIES_PATH_TEXT:
                    synchronized (this) {
                        PROPERTIES_PATH = event.getNewValue();
                    }
                    setProperties(PROPERTIES_PATH_TEXT, PROPERTIES_PATH);
                    break;
                case FFMPEG_PATH_TEXT:
                    synchronized (this) {
                        FFMPEG_PATH = event.getNewValue();
                    }
                    setProperties(FFMPEG_PATH_TEXT, FFMPEG_PATH);
                    break;
                case SAMPLE_RATE_TEXT:
                    synchronized (this) {
                        SAMPLE_RATE = event.getNewValue();
                    }
                    setProperties(SAMPLE_RATE_TEXT, SAMPLE_RATE);
                    break;
                case APIKEY_TEXT:
                    synchronized (this) {
                        apiKey = event.getNewValue();
                    }
                    setProperties(APIKEY_TEXT, apiKey);
                    break;
                case OS_PATH_TEXT:
                    synchronized (this) {
                        OS_PATH = event.getNewValue();
                    }
                    setProperties(OS_PATH_TEXT, OS_PATH);
                    break;
                case OS_CONFIG_TEXT:
                    synchronized (this) {
                        OS_CONFIG = event.getNewValue();
                    }
                    setProperties(OS_CONFIG_TEXT, OS_CONFIG);
                    break;
                case MODEL_PATH_TEXT:
                    synchronized (this) {
                        MODEL_PATH = event.getNewValue();
                    }
                    setProperties(MODEL_PATH_TEXT, MODEL_PATH);
                    break;
                case TRAIN_PATH_TEXT:
                    synchronized (this) {
                        TRAIN_PATH = event.getNewValue();
                    }
                    setProperties(TRAIN_PATH_TEXT, TRAIN_PATH);
                    break;
                case VAD_PATH_TEXT:
                    synchronized (this) {
                        VAD_PATH = event.getNewValue();
                    }
                    setProperties(VAD_PATH_TEXT, VAD_PATH);
                    break;
            }
            buildData();
        });
    }

    /**
     * Load properties from file and fill in table
     */
    private void buildData() {
        ObservableList<Property> data = FXCollections.observableArrayList();
        Platform.runLater(() -> {
            try {
                getProperties();
                PropertiesConfiguration config = new PropertiesConfiguration(PROPERTIES_PATH);
                Iterator<String> keys = config.getKeys();
                List<String> keyList = new ArrayList<>();
                while (keys.hasNext()) {
                    keyList.add(keys.next());
                    data.add(new Property(keyList.get(0), String.valueOf(config.getProperty(keyList.get(0)))));
                    keyList.remove(0);
                }
                configTable.setItems(data);
                validateConfig();
                configTable.refresh();
            } catch (ConfigurationException e) {
                logger.error("Data building error", e);
            }
            for (Property property : configTable.getItems()) {
                if (property.getStatus().getImage().impl_getUrl().contains("ok")) {
                    startButton.setDisable(false);
                } else {
                    startButton.setDisable(true);
                    isError = true;
                    break;
                }
            }
            if (!isError && FIRST_START) {
                Stage stage = (Stage) configTable.getScene().getWindow();
                stage.hide();
                if (!FileHelp.checkDirectories()) {
                    logger.error("Can't create folders");
                }
                new Main();
            }
        });
    }

    private void validateConfig() {
        for (Property property : configTable.getItems()) {
            switch (property.getKey()) {
                case APIKEY_TEXT:
                    property.setStatus(checkYoutubeAPIKey());
                    break;
                case DATABASE_PATH_TEXT:
                    property.setStatus(checkDatabase());
                    break;
                case DATA_PATH_TEXT:
                    property.setStatus(checkData());
                    break;
                case FFMPEG_PATH_TEXT:
                    property.setStatus(checkFFMPEG());
                    break;
                case OS_PATH_TEXT:
                    property.setStatus(checkOpenSMILE());
                    break;
                case PROPERTIES_PATH_TEXT:
                    property.setStatus(checkConfig());
                    break;
                case RELATEDVIDEO_COUNT_TEXT:
                    property.setStatus(checkRelatedVideoCount());
                    break;
                case SAMPLE_RATE_TEXT:
                    property.setStatus(checkSampleRate());
                    break;
                case OS_CONFIG_TEXT:
                    property.setStatus(checkOSConfig());
                    break;
                case MODEL_PATH_TEXT:
                    property.setStatus(checkModel());
                    break;
                case TRAIN_PATH_TEXT:
                    property.setStatus(checkTrain());
                    break;
                case VAD_PATH_TEXT:
                    property.setStatus(checkVAD());
                    break;
            }
        }
    }


    /**
     * Make context menu for each row in table, right click mouse
     *
     * @param contextMenu context menu object
     */
    private void createContextMenu(ContextMenu contextMenu) {
        TablePosition pos = configTable.getSelectionModel().getSelectedCells().get(0);
        int row = pos.getRow();
        String value = configTable.getItems().get(row).getKey();
        switch (value) {
            case DATABASE_PATH_TEXT:
                databaseSelected(contextMenu);
                break;
            case DATA_PATH_TEXT:
                dataSelected(contextMenu);
                break;
            case PROPERTIES_PATH_TEXT:
                configSelected(contextMenu);
                break;
            case FFMPEG_PATH_TEXT:
                ffmpegSelected(contextMenu);
                break;
            case OS_PATH_TEXT:
                osSelected(contextMenu);
                break;
            case OS_CONFIG_TEXT:
                osConfig(contextMenu);
                break;
            case MODEL_PATH_TEXT:
                modelSelected(contextMenu);
                break;
            case TRAIN_PATH_TEXT:
                trainSet(contextMenu);
                break;
            case VAD_PATH_TEXT:
                vadSelected(contextMenu);
                break;
        }
    }

    private void vadSelected(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select aed");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Choose aed file");
            chooser.setInitialDirectory(new File(PATH));
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    VAD_PATH = file.getPath();
                }
                setProperties(VAD_PATH_TEXT, VAD_PATH);
                buildData();
            }
        });
    }

    private void modelSelected(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select model");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Choose model file");
            chooser.setInitialDirectory(new File(DATA_PATH));
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    MODEL_PATH = file.getPath();
                }
                setProperties(MODEL_PATH_TEXT, MODEL_PATH);
                buildData();
            }
        });
    }

    private void trainSet(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select train set");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setInitialDirectory(new File(DATA_PATH));
            chooser.setTitle("Choose train set file");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    TRAIN_PATH = file.getPath();
                }
                setProperties(TRAIN_PATH_TEXT, TRAIN_PATH);
                buildData();
            }
        });
    }

    /**
     * openSMILE row was selected, show menu for this item
     *
     * @param contextMenu context menu object
     */
    private void osSelected(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select openSMILE");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Choose openSMILE file");
            chooser.setInitialDirectory(new File(PATH));
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    OS_PATH = file.getPath();
                }
                setProperties(OS_PATH_TEXT, OS_PATH);
                buildData();
            }
        });
    }

    /**
     * config row was selected, show menu for this item
     *
     * @param contextMenu context menu object
     */
    private void configSelected(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select config");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setInitialDirectory(new File(PATH));
            chooser.setTitle("Choose config file");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    PROPERTIES_PATH = file.getPath();
                }
                setProperties(PROPERTIES_PATH_TEXT, PROPERTIES_PATH);
                buildData();
            }
        });
    }

    /**
     * ffmpeg row was selected, show menu for this item
     *
     * @param contextMenu context menu object
     */
    private void ffmpegSelected(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select ffmpeg");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setInitialDirectory(new File(PATH));
            chooser.setTitle("Choose ffmpeg file");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    FFMPEG_PATH = file.getPath();
                }
                setProperties(FFMPEG_PATH_TEXT, FFMPEG_PATH);
                buildData();
            }
        });
    }

    /**
     * data path row was selected, show menu for this item
     *
     * @param contextMenu context menu object
     */
    private void dataSelected(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select folder");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Open Folder");
            File file = chooser.showDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    DATA_PATH = file.getPath();
                }
                setProperties(DATA_PATH_TEXT, DATA_PATH);
                buildData();
            }
        });
    }

    /**
     * database path row was selected, show menu for this item
     *
     * @param contextMenu context menu object
     */
    private void databaseSelected(ContextMenu contextMenu) {
        MenuItem newItem = new MenuItem("New DB");
        contextMenu.getItems().add(newItem);
        newItem.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setInitialDirectory(new File(DATA_PATH));
            chooser.setTitle("New database");
            File file = chooser.showSaveDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    DATABASE_PATH = file.getPath();
                }
                setProperties(DATABASE_PATH_TEXT, DATABASE_PATH);
                buildData();
            }
        });

        MenuItem openItem = new MenuItem("Open DB");
        contextMenu.getItems().add(openItem);
        openItem.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open File");
            chooser.setInitialDirectory(new File(DATA_PATH));
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    DATABASE_PATH = file.getPath();
                    DATA_PATH = file.getParent();
                }
                setProperties(DATABASE_PATH_TEXT, DATABASE_PATH);
                setProperties(DATA_PATH_TEXT, DATA_PATH);
                buildData();
            }
        });
    }

    private void osConfig(ContextMenu contextMenu) {
        MenuItem item = new MenuItem("Select config");
        contextMenu.getItems().add(item);
        item.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setInitialDirectory(new File(PATH));
            chooser.setTitle("Choose openSMILE config");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                synchronized (this) {
                    OS_CONFIG = file.getPath();
                }
                setProperties(OS_CONFIG_TEXT, OS_CONFIG);
                buildData();
            }
        });
    }
}
