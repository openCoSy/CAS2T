package com.opencosy.networktools.view.start;


import com.opencosy.networktools.model.property.Property;
import com.opencosy.networktools.view.main.Main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import static com.opencosy.networktools.utils.Constants.*;
import static com.opencosy.networktools.utils.Initialization.*;

/**
 * Start class
 */
public class Start extends Application {
    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(Start.class);

    // set path to log config file
    static {
        System.setProperty("logback.configurationFile", CONFIG + File.separator + "logback.xml");
    }

    /**
     * start
     *
     * @param args no
     * @throws Exception no
     */
    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    public synchronized void start(Stage primaryStage) {
        logger.info("Start.");
        //get absolute path to jar and directory
        String path = Start.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        try {
            path = URLDecoder.decode(path, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("Can't decode path to file", e);
        }
        String directory = new File(path).getParent();
        synchronized (this) {
            PATH = directory;
        }

        //module detection
        detectProperties(directory + File.separator + CONFIG);
        if ("".equals(DATA_PATH)) {
            synchronized (this) {
                DATA_PATH = directory;
            }
            Property.setProperties(DATA_PATH_TEXT, DATA_PATH);
        }
        detectFFMPEG(directory + File.separator + FFMPEG);
        detectOpenSMILE(directory + File.separator);
        detectAED(directory + File.separator);
        detectImages(directory);

        //delete .lock files
        delectLockFile(DATA_PATH + File.separator + RAWDATA + File.separator);

        open(primaryStage);

        primaryStage.setOnCloseRequest(event -> {
            if (!FIRST_START) {
                new Main();
            }
        });
    }

    private void open(Stage primaryStage){
        try {
            primaryStage.setTitle("Config");
            //load view
            FXMLLoader loader = new FXMLLoader();
            String fxmlFile = "/fxml/start.fxml";
            loader.setLocation(Start.class.getResource(fxmlFile));
            AnchorPane rootLayout = loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            logger.error("Can't create start window", e);
        }
    }
}