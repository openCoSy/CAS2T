package com.opencosy.networktools.view.activelearning;

import com.opencosy.networktools.model.svm.ClassifySVM;
import com.opencosy.networktools.model.svm.Model;
import com.opencosy.networktools.model.svm.Result;
import com.opencsv.CSVReader;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import weka.core.Instances;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;


public class ActiveLearningController implements Initializable {
    @FXML
    private Button humanButton;
    @FXML
    private ChoiceBox<String> methodChoice;
    @FXML
    private TextField humanTextField;
    @FXML
    private TextField svmCTextField;
    @FXML
    private TextField trainTextField;
    @FXML
    private TextField evalTextField;
    @FXML
    private TextField testTextField;
    @FXML
    private TextField outputTextField;
    @FXML
    private TextField targetThreshold;
    @FXML
    private TextField speechThreshold;
    @FXML
    private TextField percentTextField;
    @FXML
    private Button trainButton;
    @FXML
    private Button evalButton;
    @FXML
    private Button testButton;
    @FXML
    private Button outputButton;
    @FXML
    private Button startButton;

    private Model model;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        targetThreshold.setText("0.70");
        speechThreshold.setText("0.70");
        percentTextField.setText("10");
        svmCTextField.setText("0.001");
        methodChoice.getItems().add("Margin Sample");
        methodChoice.getItems().add("Random Sample");
        methodChoice.getSelectionModel().selectFirst();
        initSelectButtons();
        initStart();
    }

    private void initStart() {
        startButton.setOnAction(event -> {
            if (methodChoice.getSelectionModel().getSelectedItem().contains("Margin")) {
                classify();
            } else {
                classifyRandom();
            }
        });
    }

    private void classifyRandom() {
        List<Result> resultList = new LinkedList<>();
        evalutaTrainBase();
        int newInstances = model.getTrain().size();
        while (newInstances != 0) {
            resultList.add(new Result(model.getTrain().size(), model.getUar()));
            newInstances = model.getTrain().size();
            model = iterationRandom(model);
            newInstances = model.getTrain().size() - newInstances;
        }
        // save labeled data
        System.out.print("#Samples: [");
        for (Result result : resultList) {
            System.out.print(result.getSamples() + " ");
        }
        System.out.println("]");
        System.out.print("UA:[");
        for (Result result : resultList) {
            System.out.print(String.format("%.2f", result.getUar() * 100) + " ");
        }
        System.out.println("]");

        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(outputTextField.getText() + File.separator +
                            "train-final-tr" + targetThreshold.getText() + "-sp" + speechThreshold.getText() + "-pr"
                            + percentTextField.getText() + "-svmC" + svmCTextField.getText() + ".arff"));
            writer.write(model.getTrain().toString());
            writer.newLine();
            writer.flush();
            writer.close();
            BufferedWriter writer2 = new BufferedWriter(
                    new FileWriter(outputTextField.getText() + File.separator
                            + "unlabled-final-tr" + targetThreshold.getText() + "-sp" + speechThreshold.getText() + "-pr"
                            + percentTextField.getText() + "-svmC" + svmCTextField.getText() + ".arff"));
            writer2.write(model.getUnlabled().toString());
            writer2.newLine();
            writer2.flush();
            writer2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        model = null;
    }

    private void initSelectButtons() {
        trainButton.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open train");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                trainTextField.setText(file.getPath());
            }
        });

        evalButton.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open evaluation");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                evalTextField.setText(file.getPath());
            }
        });
        testButton.setOnAction(event -> {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Open test");
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                testTextField.setText(file.getPath());
            }
        });

        outputButton.setOnAction(event -> {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Choose output");
            File file = chooser.showDialog(new Stage());
            if (file != null) {
                outputTextField.setText(file.getPath());
            }
        });

        humanButton.setOnAction(event -> {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Choose human");
            File file = chooser.showDialog(new Stage());
            if (file != null) {
                humanTextField.setText(file.getPath());
            }
        });
    }

    private void classify() {
        String train = outputTextField.getText() + File.separator +
                "train-final-tr" + targetThreshold.getText() + "-sp" + speechThreshold.getText() + "-pr"
                + percentTextField.getText() + "-svmC" + svmCTextField.getText() + ".arff";
        Instances trainData = null;
        try {
            BufferedWriter writer = new BufferedWriter(
                    new FileWriter(train, false));
            trainData = ClassifySVM.getInstances(trainTextField.getText());
            for (int i = 0; i < trainData.size(); i++) {
                writer.write(trainData.get(i).stringValue(0) + "," + trainData.get(i).stringValue(trainData.numAttributes() - 1));
                writer.newLine();
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Result> resultList = new LinkedList<>();
        evalutaTrainBase();
        int newInstances = model.getTrain().size();
        int human = model.getHuman();

        while (newInstances != 0) {
            resultList.add(new Result(model.getTrain().size(), model.getUar()));
            human = human + model.getHuman();
            newInstances = model.getTrain().size();
            iteration(model, train);
            newInstances = model.getTrain().size() - newInstances;
        }

        // save labeled data
        System.out.print("#Samples: [");
        for (Result result : resultList) {
            System.out.print(result.getSamples() + " ");
        }
        System.out.println("]");
        System.out.print("UA:[");
        for (int i = 0; i < resultList.size(); i++) {
            System.out.print(String.format("%.2f", resultList.get(i).getUar() * 100) + " ");
        }
        System.out.println("]");
        System.out.println("Total human: " + human);


        String unlab = outputTextField.getText() + File.separator
                + "unlabled-final-tr" + targetThreshold.getText() + "-sp" + speechThreshold.getText() + "-pr"
                + percentTextField.getText() + "-svmC" + svmCTextField.getText() + ".arff";


        try {
            BufferedWriter writer2 = new BufferedWriter(
                    new FileWriter(unlab));
            writer2.write(model.getUnlabled().toString());
            writer2.newLine();
            writer2.flush();
            writer2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String targetLabel = trainData.classAttribute().value(0);

        getWav(train, outputTextField.getText() + File.separator +  targetLabel,
                outputTextField.getText()+ File.separator +  targetLabel);

        model = null;
    }

    private void getWav(String train, String wavPath, String output) {
        File dir = new File(wavPath);
        File[] wavs = dir.listFiles();
        try {
            CSVReader reader = new CSVReader(new FileReader(train));
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                for (File file : wavs) {
                    if (file.getName().equals(nextLine[0] + ".wav")) {
                        File dir2 = new File(output + File.separator + nextLine[1]);
                        if (!dir2.exists()) {
                            dir2.mkdirs();
                        }
                        Files.copy(file.toPath(), new File(dir2.toPath() + File.separator + file.getName()).toPath());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void iteration(Model model2, String train) {
        final String pathTest = testTextField.getText();
        final double target = Double.valueOf(targetThreshold.getText());
        final double speech = Double.valueOf(speechThreshold.getText());
        final int percent = Integer.valueOf(percentTextField.getText());
        model = ClassifySVM.evaluateTrainNew(model2, pathTest, target, speech, percent,
                svmCTextField.getText(), humanTextField.getText(), train);
    }

    private Model iterationRandom(Model model) {
        final String pathTest = testTextField.getText();
        final int percent = Integer.valueOf(percentTextField.getText());
        return ClassifySVM.evaluteTrainRandom(model, pathTest, svmCTextField.getText(), percent);
    }

    private void evalutaTrainBase() {
        final String pathTrain = trainTextField.getText();
        final String pathTest = testTextField.getText();
        final String pathEval = evalTextField.getText();
        model = ClassifySVM.evaluateTrain(pathTrain, pathEval, pathTest, svmCTextField.getText());
        System.out.println("UAR for first train: " + model.getUar());
    }
}
