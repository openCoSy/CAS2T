package com.opencosy.networktools.view.activelearning;

import com.opencosy.networktools.view.create.Create;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class ActiveLearning {
    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(ActiveLearning.class);
    public ActiveLearning(){
        start();
    }


    public void start() {
        Stage primaryStage = new Stage();
        primaryStage.setTitle("Active Learning");
        //load view
        FXMLLoader loader = new FXMLLoader();
        String fxmlFile = "/fxml/learning.fxml";
        loader.setLocation(ActiveLearning.class.getResource(fxmlFile));

        try {
            AnchorPane rootLayout = loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
           logger.error("Active learning", e);
        }
    }
}
