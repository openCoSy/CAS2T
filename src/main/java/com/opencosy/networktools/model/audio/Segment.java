package com.opencosy.networktools.model.audio;

public class Segment {
    private int mode;
    private String start;
    private String end;
    private String period;

    public Segment(int mode, String start, String end) {
        this.mode = mode;
        this.start = start;
        this.end = end;
    }

    public Segment(int mode, String period) {
        this.mode = mode;
        this.period = period;
    }

    public Segment(int mode) {
        this.mode = mode;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
