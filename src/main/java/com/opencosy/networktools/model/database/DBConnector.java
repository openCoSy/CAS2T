package com.opencosy.networktools.model.database;

import com.google.api.services.youtube.model.Comment;
import com.google.api.services.youtube.model.Video;
import com.opencosy.networktools.model.graph.Edge;
import com.opencosy.networktools.model.video.MyVideo;
import com.opencosy.networktools.utils.Constants;
import com.opencosy.networktools.utils.FileHelp;
import com.opencosy.networktools.view.create.Create;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigInteger;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Database class
 */
public class DBConnector {

    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(DBConnector.class);

    private Connection connection;

    private static final String YOUTUBE_KEY_TEXT = "youtubeKey";

    /**
     * Connection to database
     *
     * @param path path to database
     */
    public void connect(String path, boolean create) {
        connection = null;
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            logger.error("Can't connect to org.sqlite.JDBC", e);
        }
        File file = new File(path);
        try {
            if (file.exists() && !create) {
                connection = DriverManager.getConnection("jdbc:sqlite:" + path);
            }
            if (create && !file.exists()) {
                File dir = new File(file.getParent());
                dir.mkdirs();
                connection = DriverManager.getConnection("jdbc:sqlite:" + path);
                newDatabase();
                new Create();
            }

            if (!create && !file.exists()) {
                File dir = new File(file.getParent());
                dir.mkdirs();
                connection = DriverManager.getConnection("jdbc:sqlite:" + path);
                newDatabase();
                new Create();
            }

            if (create && file.exists()) {
                file.delete();
                File dir = new File(file.getParent());
                dir.mkdirs();
                connection = DriverManager.getConnection("jdbc:sqlite:" + path);
                newDatabase();
                new Create();
            }


        } catch (SQLException e) {
            logger.error("Can't get connection to database", e);
        }
    }

    /**
     * Close connection to database
     */
    public void closeDB() {
        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("Can't close database", e);
        }
    }

    /**
     * Create database and tables
     */
    public void newDatabase() {
        // SQL statement for creating a new table
        final String create = "CREATE TABLE IF NOT EXISTS ";
        final String autoincrement = " id integer PRIMARY KEY AUTOINCREMENT NOT NULL,";
        String[] sqlRequest = new String[7];
        sqlRequest[0] = create + "Video (\n"
                + autoincrement + "\n"
                + "	youtubeKey text NOT NULL,\n"
                + "	title text,\n"
                + "	description text,\n"
                + "	publishedAt text,\n"
                + " userId integer,\n"
                + "	categoryId integer,\n"
                + " view integer,\n"
                + "	like integer,\n"
                + "	dislike integer,\n"
                + "	date text,\n"
                + "	status integer,\n"
                + " selected integer\n"
                + ");";

        sqlRequest[1] = create + "RelatedVideo (\n"
                + autoincrement + "\n"
                + "	relatedVideoId integer,\n"
                + "	videoId integer\n"
                + ");";

        sqlRequest[2] = create + "Tags (\n"
                + autoincrement + "\n"
                + "	tag text\n"
                + ");";

        sqlRequest[3] = create + "TagsId (\n"
                + autoincrement + "\n"
                + "	tagsId integer,\n"
                + " videoId integer\n"
                + ");";

        sqlRequest[4] = create + "Category (\n"
                + autoincrement + "\n"
                + "	category text\n"
                + ");";

        sqlRequest[5] = create + "Comments (\n"
                + autoincrement + "\n"
                + "	commentKey text,\n"
                + "	videoId integer,\n"
                + "	userId integer,\n"
                + "	publishedAt text,\n"
                + "	comment text,\n"
                + "	parentId integer,\n"
                + "	like integer,\n"
                + "	date text\n"
                + ");";

        sqlRequest[6] = create + "Users (\n"
                + autoincrement + "\n"
                + "	name text\n"
                + ");";

        try {
            Statement stmt = connection.createStatement();
            // create a new table
            for (String sql : sqlRequest){
                stmt.execute(sql);
            }
            stmt.close();
        } catch (SQLException e) {
            logger.error("Can't create new database", e);
        }
    }

    /**
     * save video to database
     *
     * @param video         youtube video
     * @param relatedVideos list of related youtube videos to video
     * @param status        hide or show video in table
     * @param select        selected or not in table
     */
    public void writeVideoToDB(Video video, List<Video> relatedVideos, int status, int select) {
        final int lastVideoid = addVideo(video, status, select);
        final List<String> tags = video.getSnippet().getTags();
        if (tags != null) {
            for (String tag : tags) {
                int tagId = getTagID(tag);
                if (tagId == -1) {
                    tagId = addTag(tag);
                }
                addTagsId(lastVideoid, tagId);
            }
        }
        if (relatedVideos != null) {
            for (Video relatedVideo : relatedVideos) {
                addRelatedVideo(relatedVideo, lastVideoid);
            }
        }
    }
    /*
     * Add video to database
     */
    private int addVideo(Video video, int status, int select) {
        int lastVideoid = getVideoID(video.getId(), status);
        if (lastVideoid == -1) {
            final String queryVideo = "INSERT INTO 'Video' ('youtubeKey', 'title', 'description'," +
                    " 'publishedAt', 'view', 'like', 'dislike', 'date', 'categoryId', 'userId', 'status', 'selected'  ) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ";
            lastVideoid = addVideoRequest(video, queryVideo, status, select);
        } else {
            if (video.getSnippet().getTitle() != null) {
                updateVideo(video, status, select);
            }
        }
        return lastVideoid;
    }

    /*
     * Update existing video in database
     */
    private void updateVideo(Video video, int status, int select) {
        String query = "UPDATE Video SET title = ?, description = ?, publishedAt = ?," +
                " view = ?, like = ?, dislike = ?, date = ?, categoryId = ?, userId = ?, status = ?, selected = ? WHERE youtubeKey = ? ";
        addVideoRequest(video, query, status, select);
    }
    /*
     * Update or add new video to database. Reqeust send data to database.
     */
    private int addVideoRequest(Video video, String queryVideo, int status, int select) {
        final String youtubeKey = video.getId();
        final String title = video.getSnippet().getTitle();
        final String description = video.getSnippet().getDescription();
        final String publishedAt = video.getSnippet().getPublishedAt().toString();
        BigInteger like;
        if (video.getStatistics().getLikeCount() != null) {
            like = video.getStatistics().getLikeCount();
        } else {
            like = BigInteger.valueOf(-1);
        }

        BigInteger dislike;
        if (video.getStatistics().getLikeCount() != null) {
            dislike = video.getStatistics().getDislikeCount();
        } else {
            dislike = BigInteger.valueOf(-1);
        }

        final BigInteger view = video.getStatistics().getViewCount();
        final String category = video.getSnippet().getCategoryId();
        final String user = video.getSnippet().getChannelId();
        final String updatedTime = LocalDateTime.now().toString();

        int categoryId = getCategoryID(category);

        if (categoryId == -1) {
            categoryId = addCategory(category);
        }
        int userId = getUserId(user);

        if (userId == -1) {
            userId = addUser(user);
        }
        int lastVideoid = -1;
        try {
            PreparedStatement statement = connection.prepareStatement(queryVideo,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, youtubeKey);
            statement.setString(2, title);
            statement.setString(3, description);
            statement.setString(4, publishedAt);
            statement.setInt(5, view.intValue());
            statement.setInt(6, like.intValue());
            statement.setInt(7, dislike.intValue());
            statement.setString(8, updatedTime);
            statement.setInt(9, categoryId);
            statement.setInt(10, userId);
            statement.setInt(11, status);
            statement.setInt(12, select);
            statement.executeUpdate();

            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                lastVideoid = rs.getInt(1);
            }
            rs.close();
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't add new video", e);
        }
        return lastVideoid;
    }

    /**
     * Write one comment to database
     *
     * @param comment youtube comment
     */
    public void writeCommentToDB(Comment comment) {
        final String commentKey = comment.getId();
        String userName = comment.getSnippet().getAuthorChannelUrl();
        userName = userName.replace(Constants.PREFIX, "");
        final String publishedAt = comment.getSnippet().getPublishedAt().toString();
        final String updatedTime = LocalDateTime.now().toString();
        final String text = comment.getSnippet().getTextDisplay();
        final Long likesComment = comment.getSnippet().getLikeCount();
        final String parent = comment.getSnippet().getParentId();
        try {
            int lastCommentId = getCommentId(commentKey);
            int videoId = getVideoID(comment.getSnippet().getVideoId(), 1);
            int userId = getUserId(userName);
            if (userId == -1) {
                userId = addUser(userName);
            }
            int parentId = getCommentId(parent);
            if (lastCommentId == -1) {
                String queryVideo = "INSERT INTO 'Comments' ('commentKey', 'parentId', 'videoId', 'userId', 'publishedAt'," +
                        " 'comment', 'like', 'date' ) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?); ";

                PreparedStatement statement = connection.prepareStatement(queryVideo,
                        Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, commentKey);
                statement.setInt(2, parentId);
                statement.setInt(3, videoId);
                statement.setInt(4, userId);
                statement.setString(5, publishedAt);
                statement.setString(6, text);
                statement.setLong(7, likesComment);
                statement.setString(8, updatedTime);
                statement.executeUpdate();
                statement.close();
            }
        } catch (SQLException e) {
            logger.error("Can't write comment to database", e);
        }
    }

    /**
     * Write list of comments to database
     *
     * @param comments   list of comments
     * @param youtubeKey youtube video id
     */
    public void writeCommentToDBAll(List<Comment> comments, String youtubeKey) {
        String queryVideo = "INSERT INTO 'Comments' ('commentKey', 'parentId', 'videoId', 'userId', 'publishedAt'," +
                " 'comment', 'like', 'date' ) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?); ";
        try {
            PreparedStatement statement = connection.prepareStatement(queryVideo,
                    Statement.RETURN_GENERATED_KEYS);
            for (Comment comment : comments) {
                final String commentKey = comment.getId();
                final String publishedAt = comment.getSnippet().getPublishedAt().toString();
                final String date = LocalDateTime.now().toString();
                final String text = comment.getSnippet().getTextDisplay();
                final Long like = comment.getSnippet().getLikeCount();
                final String parent = comment.getSnippet().getParentId();
                final int lastCommentId = getCommentId(commentKey);
                final int videoId = getVideoID(youtubeKey, 1);
                final int parentId = getCommentId(parent);
                String user = comment.getSnippet().getAuthorChannelUrl();
                user = user.replace(Constants.PREFIX, "");
                int userId = getUserId(user);
                if (userId == -1) {
                    userId = addUser(user);
                }
                if (lastCommentId == -1) {
                    statement.setString(1, commentKey);
                    statement.setInt(2, parentId);
                    statement.setInt(3, videoId);
                    statement.setInt(4, userId);
                    statement.setString(5, publishedAt);
                    statement.setString(6, text);
                    statement.setLong(7, like);
                    statement.setString(8, date);
                    statement.addBatch();
                }
            }
            statement.executeBatch();
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't write comments in database", e);
        }
    }
    /*
     * Id of comment in database
     */
    private int getCommentId(String commentKey) {
        try {
            final String queryCheck = "SELECT id from Comments WHERE commentKey = ?";
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, commentKey);
            final ResultSet resultSet = ps.executeQuery();
            ps.close();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            } else {
                return -1;
            }
        } catch (SQLException e) {
            logger.error("Can't get comment id ", e);
        }
        return -1;
    }



    /**
     * Hide/show video
     *
     * @param youtubeKey video id
     * @param status     hide/show
     */
    public void changeStatus(String youtubeKey, int status) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("UPDATE Video SET status = ? WHERE youtubeKey = ? ");

            statement.setInt(1, status);
            statement.setString(2, youtubeKey);

            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't change selected video", e);
        }
    }

    private int addTag(String tag) {
        final String queryTags = "INSERT INTO 'Tags' ('tag') VALUES (?);";
        int tagId = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(queryTags,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, tag);
            statement.executeUpdate();
            ResultSet rs2 = statement.getGeneratedKeys();
            if (rs2.next()) {
                tagId = rs2.getInt(1);
            }

            statement.close();
        } catch (SQLException e) {
            logger.error("Can't add video tag", e);
        }

        return tagId;
    }
    /*
     *  Add Youtube user to database
     */
    private int addUser(String user) {
        final String queryUsers = "INSERT INTO 'Users' ('name') VALUES (?);";
        int userId = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(queryUsers,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, user);
            statement.executeUpdate();
            ResultSet rs2 = statement.getGeneratedKeys();
            if (rs2.next()) {
                userId = rs2.getInt(1);
            }
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't add user", e);
        }
        return userId;
    }

    /**
     * Add youtube category to database
     * @param category
     * @return
     */
    private int addCategory(String category) {
        final String queryCategory = "INSERT INTO 'Category' ('category') VALUES (?);";
        int categoryId = 0;
        try {
            PreparedStatement statement = connection.prepareStatement(queryCategory,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, category);
            statement.executeUpdate();
            ResultSet rs2 = statement.getGeneratedKeys();
            if (rs2.next()) {
                categoryId = rs2.getInt(1);
            }
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't add category", e);
        }
        return categoryId;
    }

    /**
     * add id of tag to database
     * @param lastVideoid
     * @param tagId
     */
    private void addTagsId(int lastVideoid, int tagId) {
        final String queryTagsId = "INSERT INTO 'TagsId' ('videoId','tagsId') VALUES (?,?);";
        try {
            PreparedStatement statement = connection.prepareStatement(queryTagsId,
                    Statement.RETURN_GENERATED_KEYS);

            statement.setInt(1, lastVideoid);
            statement.setInt(2, tagId);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't add tag's id", e);
        }
    }

    /**
     * add related video to database
     * @param relatedVideo
     * @param videoId
     */
    private void addRelatedVideo(Video relatedVideo, int videoId) {
        int lastRelatedId = addVideo(relatedVideo, 1, 0);
        if (getRelatedID(lastRelatedId, videoId) == -1) {
            try {
                String queryRelatedVideo = "INSERT INTO 'RelatedVideo' ('relatedVideoId','videoId') VALUES (?,?);";
                PreparedStatement statement = connection.prepareStatement(queryRelatedVideo,
                        Statement.RETURN_GENERATED_KEYS);

                statement.setInt(1, lastRelatedId);
                statement.setInt(2, videoId);
                statement.executeUpdate();
                statement.close();
            } catch (SQLException e) {
                logger.error("Can't add related video to database", e);
            }
        }
    }
    /**
     * Change selection state
     *
     * @param youtubeKey youtube video id
     * @param select     state
     */
    public void changeSelect(String youtubeKey, int select) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("UPDATE Video SET selected = ? WHERE youtubeKey = ? ");

            statement.setInt(1, select);
            statement.setString(2, youtubeKey);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't change selected video", e);
        }

    }

    /**
     * Multiply change selection
     *
     * @param map list of videos
     */
    public void changeSelectes(Map<String, Integer> map) {
        try {
            PreparedStatement statement = connection.prepareStatement
                    ("UPDATE Video SET selected = ? WHERE youtubeKey = ? ");
            for (Map.Entry entry : map.entrySet()) {
                statement.setInt(1, (int) entry.getValue());
                statement.setString(2, (String) entry.getKey());
                statement.addBatch();
            }
            statement.executeBatch();
            statement.close();
        } catch (SQLException e) {
            logger.error("Can't change selection of video", e);
        }
    }

    /**
     * get category from database
     * @param category
     * @return
     */
    private int getCategoryID(String category) {
        final String queryCheck = "SELECT id from Category WHERE category = ?";
        int id = -1;
        try {
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, category);
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            ps.close();
        } catch (SQLException e) {
            logger.error("Can't get category id", e);
        }
        return id;
    }

    /**
     * get user id from database
     * @param user
     * @return
     */
    private int getUserId(String user) {
        final String queryCheck = "SELECT id from Users WHERE name = ?";
        int id = -1;
        try {
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, user);
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            ps.close();
        } catch (SQLException e) {
            logger.error("Can't get category id", e);
        }
        return id;
    }

    /**
     * get tag id from database
     * @param tag
     * @return
     */
    private int getTagID(String tag) {
        final String queryCheck = "SELECT id FROM Tags WHERE tag = ?";
        int id = -1;
        try {
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, tag);
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            ps.close();
        } catch (SQLException e) {
            logger.error("Can't get tag id", e);
        }
        return id;
    }

    /**
     * get id of video in database
     *
     * @param youtubeKey Youtube id
     * @param status     hide/show status
     * @return number from database
     */
    public int getVideoID(String youtubeKey, int status) {
        int id = -1;
        final String queryCheck = "SELECT id FROM Video WHERE youtubeKey = ? AND status = ?";
        try {
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, youtubeKey);
            ps.setInt(2, status);
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            ps.close();
        } catch (SQLException e) {
            logger.error("Can't get video id", e);
        }
        return id;
    }

    /**
     * get id in database of related video
     * @param relatedVideoId
     * @param videoId
     * @return
     */
    private int getRelatedID(int relatedVideoId, int videoId) {
        final String queryCheck = "SELECT id FROM RelatedVideo WHERE relatedVideoId = ? AND videoId = ?";
        int id = -1;
        try {
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setInt(1, relatedVideoId);
            ps.setInt(2, videoId);
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            ps.close();
        } catch (SQLException e) {
            logger.error("Can't get related id", e);
        }
        return id;
    }

    /**
     * Get title of videos
     *
     * @return list of titles
     */
    public List<String> readVideoTitle() {
        List<String> idList = new LinkedList<>();
        try {
            Statement statmt = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);

            ResultSet resSet = statmt.executeQuery("SELECT DISTINCT * FROM (SELECT v.youtubeKey" +
                    " FROM Video v EXCEPT SELECT r.videoId FROM RelatedVideo r GROUP BY r.videoId);");

            while (resSet.next()) {
                String youtubeKey = resSet.getString(YOUTUBE_KEY_TEXT);
                idList.add(youtubeKey);
            }
            statmt.close();
            resSet.close();
        } catch (SQLException e) {
            logger.error("Can't read video title", e);
        }
        return idList;
    }

    /**
     * Get nodes of graph
     *
     * @return list of nodes
     */
    public List<Integer> readNode() {
        List<Integer> listVideoId = new LinkedList<>();
        try {
            Statement statmt = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet resSet = statmt.executeQuery("SELECT * FROM Video WHERE status = 1;");
            while (resSet.next()) {
                Integer id = resSet.getInt("id");
                listVideoId.add(id);
            }
            statmt.close();
            resSet.close();
        } catch (SQLException e) {
            logger.error("Can't read node", e);
        }
        return listVideoId;
    }

    /**
     * Get edge of graph
     *
     * @return list of edge
     */
    public List<Edge> readEdge() {
        List<Edge> listEdge = new LinkedList<>();
        try {
            Statement stat = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet resSet = stat.executeQuery("SELECT R.id, R.relatedVideoId, R.videoId FROM RelatedVideo R" +
                    " INNER JOIN Video V ON R.relatedVideoId = V.id WHERE V.status = 1");
            while (resSet.next()) {
                Integer relatedId = resSet.getInt("relatedVideoId");
                Integer videoId = resSet.getInt("videoId");
                listEdge.add(new Edge(videoId, relatedId));
            }
            stat.close();
            resSet.close();
        } catch (SQLException e) {
            logger.error("Can't read edge", e);
        }
        return listEdge;
    }

    /**
     * Get all videos from database
     *
     * @return list of videos
     */
    public List<MyVideo> readVideoId() {
        List<MyVideo> videos = new LinkedList<>();
        try {
            Statement stat = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet resSet = stat.executeQuery("SELECT * FROM Video WHERE status = 1;");
            while (resSet.next()) {
                final String videoId = resSet.getString(YOUTUBE_KEY_TEXT);
                final Integer select = resSet.getInt("selected");
                final Integer id = resSet.getInt("id");
                final String download = FileHelp.checkFile(videoId, 1);
                final String convert = FileHelp.checkFile(videoId, 2);
                final String vad = FileHelp.checkFile(videoId, 3);
                final String lld = FileHelp.checkFile(videoId, 4);
                final String arff = FileHelp.checkFile(videoId, 5);
                final Boolean sel = select == 1;
                videos.add(new MyVideo(id, videoId, download, convert, lld, vad, arff, sel, "0"));
            }
            stat.close();
            resSet.close();
        } catch (SQLException e) {
            logger.error("Can't read video from database", e);
        }
        return videos;
    }
}