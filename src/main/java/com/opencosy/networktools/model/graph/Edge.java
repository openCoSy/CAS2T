package com.opencosy.networktools.model.graph;

/**
 * Edge class
 */
public class Edge {
    /**
     * Start node
     */
    private Integer start;
    /**
     * End Node
     */
    private Integer end;

    /**
     * Constructor of edge
     *
     * @param start Start node
     * @param end   End node
     */
    public Edge(Integer start, Integer end) {
        this.start = start;
        this.end = end;
    }

    /**
     * getter for start node
     *
     * @return index of start node
     */
    public Integer getStart() {
        return start;
    }

    /**
     * getter for end node
     *
     * @return index of end node
     */
    public Integer getEnd() {
        return end;
    }
}
