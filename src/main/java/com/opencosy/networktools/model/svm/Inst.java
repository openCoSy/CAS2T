package com.opencosy.networktools.model.svm;

import weka.core.Instance;

/**
 * Created by strange on 13/12/2016.
 */
public class Inst {
    private Instance instance;
    private double dist;

    public Inst(Instance instance, double dist){
        this.instance = instance;
        this.dist = dist;
    }

    public Instance getInstance() {
        return instance;
    }

    public void setInstance(Instance instance) {
        this.instance = instance;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }
}
