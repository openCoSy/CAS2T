package com.opencosy.networktools.model.svm;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.SMO;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.RemoveType;

import java.io.*;
import java.util.*;

public class ClassifySVM {

    public static Model evaluateTrain(String train, String eval, String test, String c) {
        Instances trainData = getInstances(train);
        Instances testData = getInstances(test);
        Instances evalData = getInstances(eval);
        FilteredClassifier svm = makeModel(trainData, c);
        double uar = classify(svm, trainData, testData);
        return new Model(svm, trainData, evalData, uar, 0);
    }

    public static Model evaluteTrainRandom(Model model, String test, String c, int percent) {
        Instances trainData = model.getTrain();
        Instances evalData = model.getUnlabled();
        Instances testData = getInstances(test);
        FilteredClassifier classifierSMO = model.getModel();
        String targetLabel = trainData.classAttribute().value(0);
        String speechLabel = trainData.classAttribute().value(1);

        List<Integer> listInst = new LinkedList<>();

        for (int i = 0; i < evalData.size(); i++) {
            listInst.add(i);
        }

        Collections.shuffle(listInst);
        for (int i = 0; i < listInst.size() * percent / 100; i++) {
            try {
                double[] dist = classifierSMO.distributionForInstance(evalData.get(i));
                double temp3 = dist[1] - dist[0];
                if (temp3 > 0) {
                    evalData.get(i).setValue(evalData.numAttributes() - 1, speechLabel);
                    trainData.add(evalData.get(i));
                    evalData.remove(evalData.get(i));
                } else {
                    evalData.get(i).setValue(evalData.numAttributes() - 1, targetLabel);
                    trainData.add(evalData.get(i));
                    evalData.remove(evalData.get(i));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        FilteredClassifier svm = makeModel(trainData, c);
        double uar = classify(svm, trainData, testData);
        System.out.println("Unlabled: " + evalData.size());
        System.out.println("Train instances: " + trainData.size());
        System.out.println("New UAR: " + uar);
        return new Model(svm, trainData, evalData, uar, 0);

    }

    public static Model evaluateTrainNew(Model model, String test, double target, double speech, int percent, String c, String human, String output) {

        Instances trainData = model.getTrain();
        Instances evalData = model.getUnlabled();
        Instances testData = getInstances(test);
        FilteredClassifier classifierSMO = model.getModel();
        String targetLabel = trainData.classAttribute().value(0);
        String speechLabel = trainData.classAttribute().value(1);

        int counter = 0;
        try {
            List<Inst> instList = new LinkedList<>();
            List<Inst> instList2 = new LinkedList<>();
            for (int i = 0; i < evalData.numInstances(); i++) {
                if (evalData.get(i).numAttributes() > 10) {
                    double[] dist = classifierSMO.distributionForInstance(evalData.get(i));
                    double temp1 = dist[1] - dist[0];
                    if (temp1 < speech && temp1 > 0) {
                        // evalData.get(i).setValue(evalData.numAttributes() - 1, speechLabel);
                        instList.add(new Inst(evalData.get(i), dist[1]));
                    }
                    double temp2 = dist[0] - dist[1];
                    if (temp2 < target && temp2 > 0) {
                        //  evalData.get(i).setValue(evalData.numAttributes() - 1, targetLabel);
                        instList.add(new Inst(evalData.get(i), dist[0]));
                    }
                    double temp3 = dist[1] - dist[0];
                    if (temp3 > speech + 0.1) {
                        evalData.get(i).setValue(evalData.numAttributes() - 1, speechLabel);
                        instList2.add(new Inst(evalData.get(i), dist[1]));
                    }
                    double temp4 = dist[0] - dist[1];
                    if (temp4 > target + 0.1) {
                        evalData.get(i).setValue(evalData.numAttributes() - 1, targetLabel);
                        instList2.add(new Inst(evalData.get(i), dist[0]));
                    }
                }
            }


            Comparator<Inst> comparator = (o1, o2) -> {
                if (o1.getDist() > o2.getDist()) return -1;
                if (o1.getDist() < o2.getDist()) return 1;
                return 0;
            };
            instList.sort(comparator);
            instList2.sort(comparator);
            if (!Objects.equals(human, "")) {
                Instances humanTrain = getInstances(human);
                FilteredClassifier humanSVM = makeModel(humanTrain, c);
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter(output, true));
                for (Inst instance : instList) {
                    double[] dist = humanSVM.distributionForInstance(instance.getInstance());
                    double temp1 = dist[1] - dist[0];
                    if (temp1 > speech + 0.1) {
                        if (counter < instList.size() * percent / 100) {
                            instance.getInstance().setValue(evalData.numAttributes() - 1, speechLabel);
                            writer.write(instance.getInstance().stringValue(0) + ","
                                    + instance.getInstance().stringValue(instance.getInstance().numAttributes() - 1));
                            writer.newLine();
                            trainData.add(instance.getInstance());
                            evalData.remove(instance.getInstance());
                            counter++;
                        } else {
                            break;
                        }
                        // instList.add(new Inst(instance, dist[1]));
                    }
                    double temp2 = dist[0] - dist[1];
                    if (temp2 > target + 0.1) {
                        if (counter < instList.size() * percent / 100) {
                            instance.getInstance().setValue(evalData.numAttributes() - 1, targetLabel);

                            writer.write(instance.getInstance().stringValue(0) + ","
                                    + instance.getInstance().stringValue(instance.getInstance().numAttributes() - 1));
                            writer.newLine();
                            trainData.add(instance.getInstance());
                            evalData.remove(instance.getInstance());
                            counter++;
                        } else {
                            break;
                        }
                        // instList.add(new Inst(instance, dist[0]));
                    }

                }
                writer.flush();
                writer.close();
            }
            try {
                int counter2 = 0;
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter(output, true));
                for (Inst instance : instList2) {
                    if (counter2 < instList2.size() * percent / 100) {
                        writer.write(instance.getInstance().stringValue(0) + ","
                                + instance.getInstance().stringValue(instance.getInstance().numAttributes() - 1));
                        writer.newLine();
                        trainData.add(instance.getInstance());
                        evalData.remove(instance.getInstance());
                        counter2++;
                    }
                }
                writer.flush();
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        FilteredClassifier svm = makeModel(trainData, c);
        double uar = classify(svm, trainData, testData);
        System.out.println("Unlabled: " + evalData.size());
        System.out.println("Train instances: " + trainData.size());
        System.out.println("New UAR: " + uar);
        System.out.println("Labelled by human: " + counter);
        return new Model(svm, trainData, evalData, uar, counter);
    }

    private static double classify(FilteredClassifier model, Instances train, Instances test) {
        double uarValue = 0;
        try {
            Evaluation evaluation = new Evaluation(train);
            evaluation.evaluateModel(model, test);
            uarValue = uar(evaluation.confusionMatrix());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uarValue;
    }

    public static Instances getInstances(String data) {
        Instances instances = null;
        try {
            ConverterUtils.DataSource dataSource1 = new ConverterUtils.DataSource(data);
            instances = dataSource1.getDataSet();
            instances.setClassIndex(instances.numAttributes() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instances;
    }

    public static FilteredClassifier makeModel(Instances input, String c) {
        String[] options2 = new String[2];
        options2[0] = "-T";                                    // "range"
        options2[1] = "string";
        RemoveType rm = new RemoveType();
        try {
            rm.setOptions(options2);  // remove 1st attribute
        } catch (Exception e) {
            e.printStackTrace();
        }
        // classifier

        //readARFF();


        // -C $C -L $L -N 1 -M -P 1.0E-12 -V -1 -W 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"
        String[] options = new String[15];
        options[0] = "-C";
        options[1] = c;

        options[2] = "-L";
        options[3] = "0.1";

        options[4] = "-N";
        options[5] = "1";

        options[6] = "-M";

        options[7] = "-P";
        options[8] = "1.0E-12";

        options[9] = "-V";
        options[10] = "-1";

        options[11] = "-W";
        options[12] = "1";

        options[13] = "-K";
        options[14] = "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0";

        FilteredClassifier fc = new FilteredClassifier();
        try {
            SMO classifierSMO = new SMO();
            classifierSMO.setOptions(options);
            fc.setFilter(rm);
            fc.setClassifier(classifierSMO);
            fc.buildClassifier(input);
            // saveModel(classifierSMO, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fc;
    }


    private static Instances setFilter(Instances data) throws Exception {
        String[] options2 = new String[2];
        options2[0] = "-T";                                    // "range"
        options2[1] = "string";                                     // first attribute
        RemoveType remove = new RemoveType();                         // new instance of filter
        remove.setOptions(options2);                           // set options
        remove.setInputFormat(data);                          // inform filter about dataset **AFTER** setting options
        return Filter.useFilter(data, remove);   // apply filter
    }

    private static void saveModel(Classifier c, String path) throws Exception {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
        oos.writeObject(c);
        oos.flush();
        oos.close();
    }

    private static Classifier loadModel(String path) {
        Classifier classifier = null;
        try {
            FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            classifier = (Classifier) ois.readObject();
            ois.close();
            fis.close();
        } catch (EOFException e) {
            //eof - no error in this case
        } catch (IOException | ClassNotFoundException e) {
            //something went wrong
            e.printStackTrace();
        }
        return classifier;
    }

    private static double uar(double[][] matrix) {
        double value = 0;
        for (int i = 0; i < matrix.length; i++) {
            double temp = 0;
            for (int j = 0; j < matrix[0].length; j++) {
                temp = temp + matrix[i][j];
            }
            value = value + matrix[i][i] / temp;
        }
        return value / matrix.length;
    }
}
