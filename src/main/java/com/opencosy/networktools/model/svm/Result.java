package com.opencosy.networktools.model.svm;

/**
 * Created by strange on 14/12/2016.
 */
public class Result {

    private int samples;
    private double uar;

    public Result(int samples, double uar){
        this.samples = samples;
        this.uar = uar;
    }
    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
    }

    public double getUar() {
        return uar;
    }

    public void setUar(double uar) {
        this.uar = uar;
    }
}
