package com.opencosy.networktools.model.svm;

import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;


public class Model {
    private FilteredClassifier model;
    private Instances train;
    private Instances unlabled;
    private double uar;
    private int human;

    public Model(FilteredClassifier model, Instances train, Instances unlabled, double uar, int human) {
        this.model = model;
        this.train = train;
        this.unlabled = unlabled;
        this.uar = uar;
        this.human = human;
    }

    public FilteredClassifier getModel() {
        return model;
    }

    public void setModel(FilteredClassifier model) {
        this.model = model;
    }

    public Instances getTrain() {
        return train;
    }

    public void setTrain(Instances train) {
        this.train = train;
    }

    public Instances getUnlabled() {
        return unlabled;
    }

    public void setUnlabled(Instances unlabled) {
        this.unlabled = unlabled;
    }

    public double getUar() {
        return uar;
    }

    public void setUar(double uar) {
        this.uar = uar;
    }

    public int getHuman() {
        return human;
    }

    public void setHuman(int human) {
        this.human = human;
    }
}
