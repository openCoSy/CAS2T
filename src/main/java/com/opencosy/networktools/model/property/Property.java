package com.opencosy.networktools.model.property;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static com.opencosy.networktools.utils.Constants.*;

/**
 * Model of properties
 */
public class Property {

    /**
     * Log variable
     */
    private static final Logger logger = LoggerFactory.getLogger(Property.class);

    /**
     * Name of property
     */
    private SimpleStringProperty key = new SimpleStringProperty();

    /**
     * Value of property
     */
    private SimpleStringProperty value = new SimpleStringProperty();

    /**
     * Check property value
     */
    private SimpleObjectProperty<ImageView> status = new SimpleObjectProperty<>();

    /**
     * Constructor of property
     *
     * @param key   name
     * @param value value
     */
    public Property(String key, String value) {
        setKey(key);
        setValue(value);
    }

    /**
     * getter for key
     *
     * @return key
     */
    public String getKey() {
        return key.get();
    }

    /**
     * setter for key
     *
     * @param key name of parameter
     */
    public void setKey(String key) {
        this.key.set(key);
    }

    /**
     * setter for value
     *
     * @param value value of parameter
     */
    public void setValue(String value) {
        this.value.set(value);
    }

    /**
     * getter for value
     *
     * @return value of parameter
     */
    public String getValue() {
        return this.value.get();
    }

    /**
     * setter for status
     *
     * @param status status of parameter
     */
    public void setStatus(boolean status) {
        Image image;
        if (status) {
            image = new Image(new File(PATH + File.separator + IMAGES + File.separator + TICK_PATH).toURI().toString());
        } else {
            image = new Image(new File(PATH + File.separator + IMAGES + File.separator + CROSS_PATH).toURI().toString());
        }
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(20);
        imageView.setFitWidth(20);
        this.status.set(imageView);
    }

    /**
     * getter for status
     *
     * @return status of parameter
     */

    public ImageView getStatus() {
        return status.get();
    }

    /**
     * load properties from file to variables
     */
    public static void getProperties() {
        try {
            PropertiesConfiguration config = new PropertiesConfiguration(PROPERTIES_PATH);
            apiKey = String.valueOf(config.getProperty(APIKEY_TEXT));
            FFMPEG_PATH = String.valueOf(config.getProperty(FFMPEG_PATH_TEXT));
            DATA_PATH = String.valueOf(config.getProperty(DATA_PATH_TEXT));
            DATABASE_PATH = String.valueOf(config.getProperty(DATABASE_PATH_TEXT));
            PROPERTIES_PATH = String.valueOf(config.getProperty(PROPERTIES_PATH_TEXT));
            RELATEDVIDEO_COUNT = String.valueOf(config.getProperty(RELATEDVIDEO_COUNT_TEXT));
            SAMPLE_RATE = String.valueOf(config.getProperty(SAMPLE_RATE_TEXT));
            OS_CONFIG = String.valueOf(config.getProperty(OS_CONFIG_TEXT));
            MODEL_PATH = String.valueOf(config.getProperty(MODEL_PATH_TEXT));
            TRAIN_PATH = String.valueOf(config.getProperty(TRAIN_PATH_TEXT));
            VAD_PATH = String.valueOf(config.getProperty(VAD_PATH_TEXT));
        } catch (ConfigurationException e) {
            logger.error("Configuration didn't read", e);
        }
    }

    /**
     * save property from variables to file
     *
     * @param key   name of property
     * @param value value of property
     */
    public static void setProperties(String key, String value) {
        try {
            PropertiesConfiguration config = new PropertiesConfiguration(PROPERTIES_PATH);
            config.setProperty(key, value);
            config.save();
        } catch (ConfigurationException e) {
            logger.error("Configuration didn't set", e);
        }
    }
}
