package com.opencosy.networktools.model.youtube;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Lists;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeRequestInitializer;
import com.google.api.services.youtube.model.*;
import com.opencosy.networktools.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Youtube API class request
 */
public class VideoSnippetHandler {
    private YouTube youtube;
    /**
     * Log variable
     */
    private final Logger logger = LoggerFactory.getLogger(VideoSnippetHandler.class);

    /**
     * Constructor class
     */
    public VideoSnippetHandler() {
        // create single Youtube instance
        youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), httpRequest -> {
        }).setYouTubeRequestInitializer(new YouTubeRequestInitializer(Constants.apiKey))
                .setApplicationName("youtube-cmdline-search-sample").build();
    }

    /**
     * make request to Youtube and get video by id
     *
     * @param videoId Youtube video id
     * @return video object
     */
    public Video getVideo(String videoId) {
        try {
            VideoListResponse videoListResponse = youtube.videos().
                    list("snippet,id, statistics").setId(videoId).execute();
            if (!videoListResponse.getItems().isEmpty()) {
                return videoListResponse.getItems().get(0);
            }
        } catch (IOException e) {
            logger.error("Can't get video", e);
        }
        return null;
    }

    /**
     * Search for related videos to another video by id
     *
     * @param videoId
     * @param count
     * @return
     */
    private List<SearchResult> searchById(String videoId, long count) {
        // Define the API request for retrieving search results.

        SearchListResponse searchResponse = new SearchListResponse();
        try {
            YouTube.Search.List search = youtube.search().list("id,snippet");

            // Restrict the search results to only include videos. See:
            // https://developers.google.com/youtube/v3/docs/search/list#type
            search.setType("video");

            // To increase efficiency, only retrieve the fields that the
            // application uses.
            search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
            search.setMaxResults(count);
            search.setRelatedToVideoId(videoId);

            // Call the API and print results.
                searchResponse = search.execute();
        } catch (IOException e) {
            logger.error("Can't search by id", e);
        }

        return searchResponse.getItems();
    }

    /**
     * Get comments to video by id
     * @param videoId
     * @return
     */
    private YouTube.CommentThreads.List getCommentsList(String videoId) {
        long size = 100L;
        try {
            return youtube.commentThreads()
                    .list("snippet,replies,id").setVideoId(videoId).setTextFormat("plainText")
                    .setMaxResults(size).setModerationStatus("published");
        } catch (IOException e) {
            logger.error("Can't get comments list", e);
        }
        return null;
    }

    /**
     * Request list of related video to base video
     *
     * @param id    base id
     * @param count counte of related video
     * @return list of videos
     */
    public List<Video> getRelatedVideo(String id, long count) {
        List<Video> suggestionList = new LinkedList<>();
        List<SearchResult> searchResultList = searchById(id, count);
        if (searchResultList != null) {
            for (SearchResult result : searchResultList) {
                ResourceId rId = result.getId();
                // Confirm that the result represents a video. Otherwise, the
                // item will not contain a video ID.
                if ("youtube#video".equals(rId.getKind())) {
                    suggestionList.add(getVideo(rId.getVideoId()));
                }
            }
        }
        return suggestionList;
    }

    /**
     * Get comment from video
     *
     * @param videoId video id
     * @return list of comments
     */
    public List<Comment> getComments(String videoId) {
        List<Comment> commentList = new LinkedList<>();
        try {
            CommentThreadListResponse commentsPage = getCommentsList(videoId).execute();
            if (commentsPage.getItems().isEmpty()) {
                return commentList;
            }
            while (true) {
                commentList.addAll(handleCommentsThreads(commentsPage.getItems()));
                String nextPageToken = commentsPage.getNextPageToken();
                if (nextPageToken != null) {
                    commentsPage = getCommentsList(videoId).setPageToken(nextPageToken).execute();
                } else {
                    // Get next page of video comments threads
                    break;
                }
            }
        } catch (IOException e) {
            logger.error("Can't execute comments list", e);
        }
        return commentList;
    }

    /**
     * Handle of comnent threads
     * @param commentThreads
     * @return
     */
    private List<Comment> handleCommentsThreads(List<CommentThread> commentThreads) {
        List<Comment> comments = Lists.newArrayList();
        for (CommentThread commentThread : commentThreads) {
            comments.add(commentThread.getSnippet().getTopLevelComment());
            CommentThreadReplies replies = commentThread.getReplies();
            if (replies != null)
                comments.addAll(replies.getComments());
        }
        return comments;
    }
}