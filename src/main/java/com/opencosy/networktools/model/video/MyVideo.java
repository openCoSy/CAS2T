package com.opencosy.networktools.model.video;


import javafx.beans.property.*;

/*
 *Class of video in table
 */
public class MyVideo {
    /*
     * Init fields
     */
    private SimpleStringProperty videoId = new SimpleStringProperty();
    private SimpleStringProperty downloadedVideo = new SimpleStringProperty();
    private SimpleStringProperty convertedVideo = new SimpleStringProperty();
    private SimpleBooleanProperty isSelected = new SimpleBooleanProperty();
    private SimpleStringProperty lcc = new SimpleStringProperty();
    private SimpleIntegerProperty index = new SimpleIntegerProperty();
    private SimpleStringProperty vad = new SimpleStringProperty();
    private SimpleStringProperty arff = new SimpleStringProperty();
    private SimpleDoubleProperty similar = new SimpleDoubleProperty();
    private SimpleStringProperty lldVideo = new SimpleStringProperty();

    /*
     *  Constructor of line in table
     */
    public MyVideo(Integer index, String videoId, String download, String convert, String lld, String vad, String arff, Boolean select, String llc) {
        this.index.set(index);
        this.isSelected.set(select);
        this.videoId.set(videoId);
        this.lldVideo.set(lld);
        this.vad.set(vad);
        this.arff.set(arff);
        this.downloadedVideo.set(download);
        this.convertedVideo.set(convert);
        this.lcc.set(llc);
    }


    // Setters and getters

    public String getVideoId() {
        return videoId.get();
    }

    public void setVideoId(String id) {
        this.videoId.set(id);
    }

    public String getDownloadedVideo() {
        return downloadedVideo.get();
    }

    public void setDownloadedVideo(String download) {
        this.downloadedVideo.set(download);
    }

    public String getConvertedVideo() {
        return convertedVideo.get();
    }

    public void setConvertedVideo(String path) {
        this.convertedVideo.set(path);
    }

    public Boolean getSelected() {
        return isSelected.get();
    }

    public void setSelected(Boolean selected) {
        this.isSelected.set(selected);
    }

    public BooleanProperty isSelectedProperty() {
        return this.isSelected;
    }

    public String getLcc() {
        return lcc.get();
    }

    public SimpleStringProperty lccProperty() {
        return lcc;
    }

    public void setLcc(String lcc) {
        this.lcc.set(lcc);
    }

    public int getIndex() {
        return index.get();
    }

    public SimpleIntegerProperty indexProperty() {
        return index;
    }

    public void setIndex(int index) {
        this.index.set(index);
    }

    public String getVad() {
        return vad.get();
    }

    public SimpleStringProperty vadProperty() {
        return vad;
    }

    public void setVad(String vad) {
        this.vad.set(vad);
    }

    public String getArff() {
        return arff.get();
    }

    public SimpleStringProperty arffProperty() {
        return arff;
    }

    public void setArff(String arff) {
        this.arff.set(arff);
    }

    public double getSimilar() {
        return similar.get();
    }

    public SimpleDoubleProperty similarProperty() {
        return similar;
    }

    public void setSimilar(double similar) {
        this.similar.set(similar);
    }

    public String getLldVideo() {
        return lldVideo.get();
    }

    public SimpleStringProperty lldVideoProperty() {
        return lldVideo;
    }

    public void setLldVideo(String lldVideo) {
        this.lldVideo.set(lldVideo);
    }
}
